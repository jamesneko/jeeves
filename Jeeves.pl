#!/usr/bin/perl -CSDA

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

package Jeeves;
use base qw( Bot::BasicBot );

use FindBin qw($RealBin);
use lib "$RealBin/libs";

use Getopt::Long;
use Term::ANSIColor;
use Data::Dumper;
use DateTime;
use Encode qw/encode_utf8 decode_utf8/;

use Jeeves::Memory;
use Jeeves::PluginSystem;
use Jeeves::Cooldown qw/cooldown resetting_cooldown cooldown_time_remaining/;

use Jeeves::Command;
use Jeeves::Substitution;
use Jeeves::Emote;
use Jeeves::Conversational;

my %opts = (
		nick => 'Jeeves',
		cookies => "$RealBin/mem/cookies.json",
	);
GetOptions(\%opts, "server=s", "channel=s@", "nick=s", "cookies=s");

Jeeves::Memory::load_cookies($opts{cookies});
Jeeves::PluginSystem::load_plugins("$RealBin/libs/Jeeves/Plugin/", "Jeeves::Plugin");

my @modules;
push @modules, Jeeves::Command->new();
push @modules, Jeeves::Substitution->new();
push @modules, Jeeves::Emote->new();
push @modules, Jeeves::Conversational->new();	# Conversational should be the last module, for the 'I don't understand' response.


sub help
{
	return "I am Jeeves, the most technologically advanced robot butler in all of Azeroth. I can repair your items and grant bank access to sufficiently skilled Engineers.";
}


sub r(@)
{
	return $_[int rand(@_)];
}


sub connected
{
	my ($self) = @_;
	say colored("Connected to " . $self->server() . ".", 'bold green');
}


sub said
{
	my ($self, $message) = @_;
	my ($channel, $who, $address, $body) = @{$message}{qw/channel who address body/};
	
	print	colored($message->{channel}, 'bold white'),
			colored(" <", 'green'),
			colored($message->{who}, 'bold green'),
			colored("> ", 'green'),
			$message->{address}? colored($message->{address} . ": ", 'bold magenta') : "",
			colored($message->{body}, 'white'),
			"\n";
	

	# New reloadable plugins
	#### FOR THE MOMENT these have to come before the old fixed modules,
	   # Because guess what? You left a catch-all in there!
	my $plugin_list = Jeeves::PluginSystem::get_plugins();
	my $rval = $plugin_list->said($self, $channel, $who, $address, $body);
	return if $rval && $rval eq "NO_RESPONSE";
	return $rval if $rval;


	# Old fixed modules
	foreach my $module (@modules) {
		my $rval = $module->said($self, @{$message}{qw/channel who address body/});
		return if $rval && $rval eq "NO_RESPONSE";
		return $rval if $rval;
	}
	
	
	return;
}


sub noticed
{
	my ($self, $message) = @_;
	print	colored($message->{channel}, 'bold white'),
			colored(" NOTICE:", 'bold blue'),
			colored(" <", 'green'),
			colored($message->{who}, 'bold green'),
			colored("> ", 'green'),
			$message->{address}? colored($message->{address} . ": ", 'bold magenta') : "",
			colored($message->{body}, 'white'),
			"\n";
	
	return;
}


my $flood_protection_counter = 0;
my $rapid_message_counter = 0;
sub say
{
	my $self = shift;
	my $args;
	if (ref $_[0] eq 'HASH') {
		$args = shift;
	} else {
		%$args = @_;
	}
	#say colored("DEBUG: say(@_): ", 'bold yellow'), Dumper($args);
	
	# Protect against excessively enormous messages - except if we are sending a private message.
	if ($args->{channel} && $args->{channel} ne "msg") {
		$args->{body} = "That's quite a tongue-twister." if length $args->{body} > 600;
		$args->{body} = substr($args->{body}, 0, 290) . "\N{HORIZONTAL ELLIPSIS}" if length $args->{body} > 290;
	}
		
	# Flood prevention for saying the same thing repeatedly.
	if ( ! resetting_cooldown("say:" . $args->{body}, 4)) {
		if (cooldown("warn_flood", 30)) {
			say colored("Flood Protection: Won't say the same thing repeatedly ($args->{body}).", 'bold blue');
			if ($flood_protection_counter >= 1) {
				# It doesn't seem to be going down!
				say colored("Flood Protection: Consistent problem, shutting down.", 'bold blue');
				$self->SUPER::say({ channel => $args->{channel}, body => "If you'll excuse me, sirs, I feel most unwell and must shut down." });
				$self->shut_down_gracefully("Flood protection");
			}
			$flood_protection_counter++;
		}
		return;
	}
	if ($flood_protection_counter > 0 && cooldown("flood_subsiding", 300)) {
		$flood_protection_counter--;
	}
	
	# Flood prevention for saying a lot of different things in a very short timespan.
	# (Break bot-to-bot conversation cycles)
	if ( ! resetting_cooldown("rapid_message", 1)) {
		$rapid_message_counter++;
		if ($rapid_message_counter >= 7) {
			say colored("Flood Protection: Rapid message spam.", 'bold blue') if cooldown("warn_rapid_message", 5);
		}
	}
	if (cooldown_time_remaining("warn_rapid_message", 5)) {
		return;
	}
	
	# Show the console what we're saying.
	print	colored($args->{channel}, 'bold white'),
			colored(" <", 'yellow'),
			colored($self->nick(), 'bold yellow'),
			colored("> ", 'yellow');
	print colored("(NOTICE) ", 'bold blue') if ($args->{irc_command} // "" eq 'notice');
	print	colored("(to ", 'green'),
			colored($args->{who}, 'bold green'),
			colored(") ", 'green') if $args->{who};
	print	$args->{address}? colored($args->{address} . ": ", 'bold magenta') : "",
			colored($args->{body}, 'white'),
			"\n";
	
	# Allow people to trick us into saying things in #idlerpg, but not repeatedly.
	if ($args->{channel} =~ /idlerpg/) {
	  if (resetting_cooldown("#idlerpg", 600)) {
	    $self->SUPER::say($args);
	  }
	  # And comment on the fact
	  if (resetting_cooldown("#idlerpg_tricked_me", 590)) {
	    # I -think- this will queue a message?
	    $self->SUPER::say({ %$args, body => r("Oh, blast.", "Drat.", "Oh fiddlesticks.") });
	  }
	  return;
	}

	# Actually say it.
	$self->SUPER::say($args);
}


sub kicked
{
	my ($self, $kicked) = @_;
	say colored("Kicked: $kicked->{who} kicked $kicked->{kicked} from $kicked->{channel} for reason $kicked->{reason}.", 'bold red');
	if ($kicked->{kicked} eq $self->nick()) {
		say colored("Wait, that's me!", 'bold red');
		$self->shut_down_gracefully("I'll see myself out.");
	}
	return;
}


sub chanjoin
{
	my ($self, $message) = @_;
	print	colored($message->{channel}, 'bold white'),
			colored(" <", 'blue'),
			colored($message->{who}, 'bold blue'),
			colored("> ", 'blue');
	print	colored("joined", 'bold cyan'),
			"\n";
	return;
}


sub chanpart
{
	my ($self, $message) = @_;
	print	colored($message->{channel}, 'bold white'),
			colored(" <", 'blue'),
			colored($message->{who}, 'bold blue'),
			colored("> ", 'blue');
	print	colored("parted", 'cyan'),
			"\n";
	return;
}


sub shut_down_gracefully
{
	my ($self, $reason) = @_;
	$reason //= $self->quit_message();
	say colored("Shutting down...", 'bold red');
	Jeeves::Memory::save_cookies($opts{cookies});

	$self->shutdown($reason);
}


my $exit_status = 0;
sub set_exit_status
{
	my ($status_code) = @_;
	$exit_status = $status_code;
}


sub morning
{
	my ($self) = @_;
	my $datetime = DateTime->now(time_zone => 'Australia/Sydney');
	return unless $datetime->hour == 9 && $datetime->minute <= 2;
	return unless cooldown('morning', 7200);
	
	# "Process each module's morning messages" except there's only one for now
	my $plugin_list = Jeeves::PluginSystem::get_plugins();
	my @morning_messages = $plugin_list->morning($self);
	
	if (@morning_messages) {
		$self->say({ channel => "#anb", body => "Good Morning. " . join(" ", @morning_messages), irc_command => 'notice' });
	}
}


sub every_10m
{
	my ($self) = @_;
	return unless ((localtime())[1] % 10 == 0 && cooldown('every_10m', 530));
	
	# Process module periodic things
	my $plugin_list = Jeeves::PluginSystem::get_plugins();
	my @periodic_messages = $plugin_list->every_10m($self);
	
	if (@periodic_messages) {
		$self->say({ channel => "#anb", body => join(" ", @periodic_messages), irc_command => 'notice' });
	}
}


sub tick
{
	my ($self) = @_;
	Jeeves::Memory::save_cookies_if_necessary($opts{cookies});
	$self->morning();
	$self->every_10m();
	return 60;
}


# Run.
die "Sir, please provide me with the address of the server you'd like me to connect to using --server.\n" unless $opts{server};
die "Sir, please provide me with the channel name I should be joining using --channel.\n" unless $opts{channel};

my $bot = Jeeves->new(
		nick => $opts{nick},
		server => $opts{server},
		channels => [ map { s/^(?!#)/#/r; } @{$opts{channel}} ],
		quit_message => "If you'll excuse me, I have pressing engagements elsewhere.",
	);
$bot->run();
exit $exit_status;

