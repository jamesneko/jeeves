package WowGuildApi;

use warnings;
use strict;
use v5.18;
use Data::Dumper;

use JSON;
use LWP::Authen::OAuth2;
use Method::Signatures;


our $AUTHURL = 'https://us.battle.net';
our $BASEURL = 'https://us.api.blizzard.com';
our $REDIRECT_URI = 'http://blog.lazycat.com.au/jeeves-anb';


method new($class:, $client_id, $client_secret, $save_tokens_callback?, $saved_tokens?)
{
  $save_tokens_callback //= sub { say "save_tokens called: " . $_[0] };
  my @saved_tokens_opt = $saved_tokens ? (token_string => $saved_tokens) : ();
  
  my $self = bless {
      oauth2 => LWP::Authen::OAuth2->new(
        authorization_endpoint => "$AUTHURL/oauth/authorize",
        token_endpoint => "$AUTHURL/oauth/token",
        client_id => $client_id,
        client_secret => $client_secret,
        redirect_uri => $REDIRECT_URI,
        scope => 'wow.profile',

        save_tokens => $save_tokens_callback,
        @saved_tokens_opt,
        
        request_required_params => [qw/grant_type client_id client_secret code redirect_uri/],
      ),
      client_id => $client_id,
      client_secret => $client_secret,
    }, $class;
  say "authorization url is: " . $self->oauth2->authorization_url();
  say 'go there and get the code, then ->set_code($code)';
  return $self;
}


method oauth2()
{
  return $self->{oauth2};
}


method token_string()
{
  return $self->oauth2->token_string;
}


method get_auth_url()
{
  return $self->oauth2->authorization_url();
}


method set_code($code)
{
  $self->{code} = $code;
  say "Requesting tokens";

  $self->oauth2->request_tokens(code => $code);
  say "Tokens are: " . $self->oauth2->token_string;
  
  say "Logged in as: " . $self->whoami();
}


method whoami()
{
  my $res = $self->oauth2->get("$AUTHURL/oauth/userinfo");
  return $res->status_line . " " . $res->decoded_content;
}


method check_token($token = $self->token_string)
{
  my $res = $self->oauth2->post("$AUTHURL/oauth/check_token", { token => $token });
  return JSON->new->utf8(0)->decode( $res->decoded_content ), $res;
}


# Uses deprecated Community API https://develop.battle.net/documentation/api-reference/world-of-warcraft-community-api
# Oddly requires guild name to be A New Beginning but returns 'A new beginning'
# Doesn't recognise Remulos.
method get_guild_roster_legacy($realm = 'Nagrand', $guild = 'A New Beginning')
{
  my $res = $self->oauth2->get("$BASEURL/wow/guild/$realm/$guild", {
      region => 'us',
      namespace => 'profile-us',
      locale => 'en_US',
      fields => 'members',
      # fields => 'news' or fields => 'members' doesn't seem to work anymore? or it does in the web client.
    });
  return JSON->new->utf8(0)->decode( $res->decoded_content ), $res;
}


# Uses deprecated Community API https://develop.battle.net/documentation/api-reference/world-of-warcraft-community-api
method get_user_characters()
{
  my $res = $self->oauth2->get("$BASEURL/wow/user/characters", {
      region => 'us',
      namespace => 'profile-us',
      locale => 'en_US',
      # fields => 'news' or fields => 'members' doesn't seem to work anymore
    });
  return JSON->new->utf8(0)->decode( $res->decoded_content ), $res;
}


# Uses new Profile API https://develop.battle.net/documentation/api-reference/world-of-warcraft-profile-api
# which just gives me a 403 Forbidden for everything.
method get_guild_roster($realm = 'Nagrand', $guild = 'A New Beginning')
{                                    #### v- why not 'profile' ?
  my $res = $self->oauth2->get("$BASEURL/data/wow/guild/$realm/$guild/roster", {
      region => 'us',
      namespace => 'profile-us',
      locale => 'en_US',
    });
  return JSON->new->utf8(0)->decode( $res->decoded_content ), $res;
}


# Uses new Profile API https://develop.battle.net/documentation/api-reference/world-of-warcraft-profile-api
# which just gives me a 403 Forbidden for everything.
method get_character_info($realm = 'Nagrand', $charname = 'Worgentleman')
{
  my $res = $self->oauth2->get("$BASEURL/profile/wow/character/$realm/$charname", {
      region => 'us',
      namespace => 'profile-us',
      locale => 'en_US',
    });
  return JSON->new->utf8(0)->decode( $res->decoded_content ), $res;
}


1;

