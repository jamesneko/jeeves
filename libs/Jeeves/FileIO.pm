package Jeeves::FileIO;
#
# $Id: FileIO.pm 695 2015-01-26 12:50:47Z james $
#
# Helper functions to dump files.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';
use Carp;

require Exporter;
our @ISA = qw/Exporter/;
# use @EXPORT_OK to list the symbols to export on request only. @EXPORT is everything that gets exported by default.
our @EXPORT_OK = qw/slurp_file spew_file/;
our %EXPORT_TAGS = ( all => [@EXPORT_OK] );


sub slurp_file($)
{
	my ($filename) = @_;

	open(my $fh, '<:utf8', $filename) or die "Couldn't read from '$filename' : $!\n";
	local $/ = undef;	# unset record separator to slurp entire file in one readline call.
	my $contents = readline $fh;
	close($fh) or die "Couldn't close file '$filename' : $!\n";
	
	return $contents;
}


sub spew_file($$)
{
	my ($filename, $contents) = @_;
	
	open(my $fh, '>:utf8', $filename) or die "Couldn't write to '$filename' : $!\n";
	print $fh $contents or die "Couldn't write to '$filename' : $!\n";
	close($fh) or die "Couldn't close file '$filename' : $!\n";
}


1;
