package Jeeves::Memory;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

require Exporter;
our @ISA = qw/Exporter/;
# use @EXPORT_OK to list the symbols to export on request only. @EXPORT is everything that gets exported by default.
our @EXPORT_OK = qw/canonical_name sir set_sir known_dates set_known_dates 
                    urltitle_hostnames set_urltitle_hostnames
                    currency_codes set_currency_codes openexchangerates_api_key
                    alphavantage_api_key
                    wow_client_id wow_client_secret wow_tokens set_wow_tokens wow_characters set_wow_characters
                    remember_unhandled_message get_pref get_pref_is_true set_pref list_pref
                    load_cookies save_cookies cookie/;
our %EXPORT_TAGS = ( all => [@EXPORT_OK] );

use JSON;               # libjson-perl
use Term::ANSIColor;

use Jeeves::FileIO qw/:all/;


our $COOKIES = {};
our $SAVED_REVISION = 0;		# What revision is on disk.


# ---- Specific to Jeeves ----

# For IRC if people time out and rejoin - strip trailing 1 and _s.
sub canonical_name($)
{
	return $_[0] =~ s/[\d_]+$//r;
}


# Sir, Madam, or 'My Liege'?
sub sir($)
{
	my ($who) = @_;
	return cookie('sir')->{canonical_name($who)} // "Sir";
}

sub set_sir($$)
{
	my ($who, $sir) = @_;
	cookie('sir')->{canonical_name($who)} = $sir;
	increment_revision();
}


# Birthdays and the like.
sub known_dates($)
{
	my ($date) = @_;
	return cookie('known_dates')->{$date} // [];
}

sub set_known_dates($$)
{
	my ($date, $newdates) = @_;
	cookie('known_dates')->{$date} = $newdates;
	increment_revision();
}


# What hostnames should trigger the UrlTitle plugin.
sub urltitle_hostnames()
{
	my $arrayref = cookie('urltitle')->{hostnames} // [];
	return @$arrayref;
}

sub set_urltitle_hostnames(@)
{
	my (@hostnames) = @_;
	cookie('urltitle')->{hostnames} = [ @hostnames ];
	increment_revision();
}


# What currency codes mentioned in chat should trigger the Currency plugin.
# We don't just let all codes in because e.g. there's a CVE code.
sub currency_codes()
{
	my $arrayref = cookie('currency')->{codes} // [];
	return @$arrayref;
}

sub set_currency_codes(@)
{
	my (@codes) = @_;
	cookie('currency')->{codes} = [ @codes ];
	increment_revision();
}

sub openexchangerates_api_key()
{
	my $apikey = cookie('currency')->{openexchangerates_api_key} // '';
	return $apikey;
}


sub alphavantage_api_key()
{
	my $apikey = cookie('currency')->{alphavantage_api_key} // '';
	return $apikey;
}


sub wow_tokens
{
	my $tokens = cookie('wowapi')->{tokens} // '';
	return $tokens;
}
sub set_wow_tokens
{
	my ($tokens) = @_;
	cookie('wowapi')->{tokens} = $tokens;
	increment_revision();
}


sub wow_client_id
{
	my $apikey = cookie('wowapi')->{client_id} // '';
	return $apikey;
}
sub wow_client_secret
{
	my $apikey = cookie('wowapi')->{client_secret} // '';
	return $apikey;
}


sub wow_characters
{
	my $characters = cookie('wowapi')->{characters} // {};
	return $characters;
}


sub set_wow_characters
{
	my ($characters) = @_;
	cookie('wowapi')->{characters} = $characters;
	increment_revision();
}


# Record keeping for messages directed at us that we didn't understand.
# Keeps the last 20.
sub remember_unhandled_message($$)
{
	my ($who, $message) = @_;
	$message = "<$who> $message";
	
	cookie('bu_dong')->{history} //= [];
	my $bu_dong = cookie('bu_dong')->{history};
	push @$bu_dong, $message;
	shift @$bu_dong if (@$bu_dong > 20);
	increment_revision();
}


# Configurable settings.
sub get_pref($)
{
	my ($prefname) = @_;
	return cookie('prefs')->{$prefname};
}


sub get_pref_is_true($)
{
	my ($prefname) = @_;
	return cookie('prefs')->{$prefname} !~ /^(0|off|no|false)$/i;
}


sub set_pref($$)
{
	my ($prefname, $value) = @_;
	cookie('prefs')->{$prefname} = $value;
	increment_revision();
}


sub list_pref()
{
	my $hash = {};
	%$hash = %{ cookie('prefs') };
	return $hash;
}


# ---- Shamelessly copied from SysadminD ----

sub load_cookies($)
{
	my ($filename) = @_;
	if (-f $filename) {
		say colored("Loading cookies from file '$filename'.", 'yellow');
		my $json_text = slurp_file($filename);
		$COOKIES = JSON->new->decode($json_text);
		$SAVED_REVISION = revision();
		
	} else {
		say colored("No cookie file '$filename' found, assuming blank slate.", 'yellow');
		$COOKIES = {};
		$SAVED_REVISION = 0;
	}
}


sub save_cookies($)
{
	my ($filename) = @_;
	say colored("Saving cookies to file '$filename'.", 'yellow');
	my $json_text = JSON->new->pretty->encode($COOKIES);
	spew_file($filename, $json_text);
	$SAVED_REVISION = revision();
}


sub save_cookies_if_necessary($)
{
	my ($filename) = @_;
	if (revision() > $SAVED_REVISION) {
		save_cookies($filename);
	}
}


sub revision()
{
	return $COOKIES->{revision} // 0;
}


sub increment_revision()
{
	$COOKIES->{revision} = revision() + 1;
}


sub cookie($)
{
	my ($name) = @_;
	$COOKIES->{$name} //= {};
	return $COOKIES->{$name};
}


1;
