#
# Abstract base for Jeeves plugins.
# Not using Bot::BasicBot::Pluggable because I want more control over loading and reloading modules;
# Not using e.g. Module::Pluggable because I want control over module ordering, and Module::Pluggable::Ordered seems like overkill.
# So hey! Let's just write our own damn plugin system, with blackjack, and hookers.
#
package Jeeves::Plugin;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use Moose;


sub loaded($$)
{
	my ($self, $pluginname) = @_;
}

sub unloaded($$)
{
	my ($self, $pluginname) = @_;
}

sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	return;
}

sub morning($$)
{
	my ($self, $bot) = @_;
	return;
}

sub every_10m($$)
{
	my ($self, $bot) = @_;
	return;
}




__PACKAGE__->meta->make_immutable;
1;

