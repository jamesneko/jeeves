package Jeeves::Conversational;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use Moose;	# Because I am lazy.

use Jeeves::Memory qw/sir set_sir remember_unhandled_message/;
use Jeeves::Cooldown qw/cooldown resetting_cooldown cooldown_time_remaining/;


my $GREETZ = qr/\b(hello|hi|hai|ohai|heya|greetings|hey|hey there)\b/i;
my $THANX = qr/\b(thanks|thank you( very much)?|ty|thx)\b/i;
my $WELCONE = qr/\b(welcome|welcone|heredue)\b/i;


sub r(@)
{
	return $_[int rand(@_)];
}


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my $sir = sir($who);
	
   if ($who =~ /(idlerpg)/i) {
   	if ($said =~ /has attained level/) {
   		return "NO_RESPONSE" unless int rand 100 < 20;
   		return r("Huzzah!", "Hooray!", "Excellent!");
   	}
		return "NO_RESPONSE";
		
   } elsif (	$address && $said =~ /${GREETZ}.?$/i
		|| $said =~ /${GREETZ}.{0,3}jeeves/i ) {
		return "Hello, " . sir($who) . "!";

	} elsif (	$address && $said =~ /${THANX}.?$/i
				|| $said =~ /${THANX}.{0,3}jeeves/i ) {
		return "My pleasure, " . sir($who) . ".";

	} elsif (	$address && $said =~ /${WELCONE}.?$/i
				|| $said =~ /${WELCONE}.{0,3}jeeves/i ) {
		return "NO_RESPONSE";

	} elsif (	$address && $said =~ /\bsorry\b.?$/i
				|| $said =~ /\bsorry\b.{0,3}jeeves/i ) {
		return "That's quite alright, " . sir($who) . ".";

	} elsif (	$address && $said =~ /^(who are you|what are you).?$/i
				|| $said =~ /\bwho is jeeves\b/i ) {
		return $bot->help();

	} elsif ($address && $said =~ /^(who created you|who is your creator).?$/i) {
		return "Neko is my creator.";

	} elsif ($address && $said =~ /^(source code|where is your source).?$/i) {
		return "My source code is hosted on Bitbucket, at https://bitbucket.org/jamesneko/jeeves/";

	} elsif ($address && $said =~ /^(got any|do you have any) grapes.?$/i) {
		if (resetting_cooldown("got_any_grapes_1", 30)) {
			return "Regretfully, " . sir($who) . ", I do not have any grapes. However, I have been programmed to serve lemonade.";
		} elsif (resetting_cooldown("got_any_grapes_2", 30)) {
			return "As I mentioned earlier, " . sir($who) . ", I presently do not have any grapes. Please accept my apologies.";
		} elsif (resetting_cooldown("got_any_grapes_3", 30)) {
			return "Haha, what a merry jest, " . sir($who) . ". Clearly you are already aware that I do not possess any grapes, and yet you persist in asking. Most amusing.";
		} elsif (resetting_cooldown("got_any_grapes_4", 30)) {
			return "Do you have an extra GOTO 10 line? I clearly stated that I don't have any grapes.";
		} else {
			$bot->emote({ who => $who, channel => $channel, body => "throws an OutOfGrapes exception at $who" });
			return "NO_GRAPES!";
		}
		
	} elsif ($address && $said =~ /^(please )?(got any|can I have|I'd like|(could you )?(serve|get|give|fetch) (?<me>\w+)) ?(some|a glass of)? lemonade.?$/i) {
		my $target = $who;
		$target = $+{me} if ($+{me} && $+{me} ne 'me');
		$bot->emote({ who => $target, channel => $channel, body => "serves a tall glass of sparkling lemonade to $target" });
		return "Here you go, " . sir($target) . ".";

	} elsif ($address && $said =~ /^(fetch|bring)(?<me> (me|us|everyone|everybody))? (?<targetobj>.*?)[,.!]?$/i) {
		my $target = $who;
		my $targetobj = $+{targetobj};
		if ($targetobj) {
			$bot->emote({ who => $target, channel => $channel, body => "dutifully fetches $targetobj" });
			return "Here you go, $sir.";
		} else {
			return "What exactly should I be fetching, $sir?";
		}

	} elsif ($address && $said =~ /^pat (?<username>.*?)[.,!?]?$/i) {
		$bot->emote({ who => $who, channel => $channel, body => "gently pats $+{username}" });
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^slap (?<username>.*?)[.,!?]?$/i) {
		$bot->emote({ who => $who, channel => $channel, body => "slaps $+{username} across the face. Ouch!" });
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^kill (?<username>.*?)[.,!?]?$/i) {
		return "$sir, while I must warn you that I am not Three Laws compliant, I am not particularly well versed in combat techniques and can see no benefit in killing $+{username}.";
		
	} elsif ($address && $said =~ /\brepair.*armou?r\b/i) {
		return "Your armour looks fine, " . sir($who) . ".";

	} elsif ($said =~ /\bdecapitates Jeeves\b/i) {
		return r( "Please don't do that, " . sir($who) . ", I'm very expensive.",
		          "Running in 'detached HEAD' state.",
		        );

	} elsif ($said =~ /┻━┻/) {
		#if (resetting_cooldown("tableflip_1", 300)) {
		#	return "┬─┬ノ(°_°ノ)";
		#} elsif (resetting_cooldown("tableflip_3", 300)) {
		#	return "(°_°”)・・・";
		
		return "NO_RESPONSE";
		
	} elsif ($address && $said =~ /\b(I'm|I am) (no|not|not a) ['"]?(Sir)['"]?\.?$/i) {
		return "Then how should I address you?";

	} elsif ($address && $said =~ /\b(you may )?(address me( as)?|call me) ['"]?(?<as>.*?)['"]?\.?$/i) {
		set_sir($who, $+{as});
		return "Very good, " . sir($who) . ", I shall address you as " . sir($who) . " in the future.";

	} else {
		# Final catch-all, assuming Conversational is last module.
		# Only trigger if addressed directly.
		return unless $address;
		# Remember the message in our cookies for later analysis.
		remember_unhandled_message($who, $said);
		
		# Do a generic "Huh?" first, then if pushed, elaborate.
		if (cooldown("do_not_understand_1", 30)) {
			return r(
					"I'm sorry, $sir, I don't understand.",
					"I'm sorry, $sir, I don't quite follow.",
					"Terribly sorry, $sir, I don't understand.",
					"I don't quite grasp your meaning, $sir.",
					"Alas, I don't understand you, $sir.",
					"$sir, I'm afraid I don't understand.",
					"I'm afraid I don't comprehend you, $sir.",
				);
		} elsif (cooldown("do_not_understand_2", 30)) {
			return "NO_RESPONSE" if $who =~ /^(hogger|idlerpg)/i;
			return "Apologies, " . sir($who) . ", but my creator has not yet completed my language module and I don't know what to say. If you are looking for intellectually stimulating conversation, may I direct you to hogger?";
		}
		return "NO_RESPONSE";	# eat any further unhandled messages, no loops even for unknown bots.
	}
	return;
}


__PACKAGE__->meta->make_immutable;
1;
