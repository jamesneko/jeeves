package Jeeves::Emote;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use Moose;	# Because I am lazy.


# Big list of everything, include \btarget\b or don't.
my @EMOTES = (
		agree => "agrees",
		agree => "agrees with target",
		amaze => "is amazed!",
		amaze => "is amazed by target",
		angry => "raises his fist in anger",
		angry => "raises his fist in anger at target",
		apologize => "apologises to everyone. Sorry!",
		apologise => "apologises to everyone. Sorry!",
		apologize => "apologises to target. Sorry!",
		apologise => "apologises to target Sorry!",
		applaud => "applauds. Bravo!",
		applaud => "applauds at target. Bravo!",
		applause => "applauds. Bravo!",
		applause => "applauds at target. Bravo!",
		attacktarget => "tells everyone to attack something",
		attacktarget => "tells everyone to attack target",
		
		bark => "barks. Woof woof!",
		bark => "barks at target",
		bashful => "is bashful",
		bashful => "is so bashful... too bashful to get target's attention",
		beckon => "beckons everyone over to him",
		beckon => "beckons target over",
		beg => "begs everyone around him. How pathetic",
		beg => "begs target. How pathetic",
		belch => "lets out a loud belch",
		belch => "burps rudely in target's face",
		bite => "looks around for someone to bite",
		bite => "bites target. Ouch!",
		bleed => "oozes blood from ... wait, where exactly?",
		bleed => "fetches leeches to bleed target",
		blink => "blinks his optical receptors",
		blink => "blinks at target",
		blow => "blows a kiss into the wind",
		blow => "blows a kiss at target",
		blush => "blushes",
		blush => "blushes at target",
		boggle => "boggles at the situation",
		boggle => "boggles at target",
		bonk => "bonks himself on the noggin. Doh!",
		bonk => "bonks target on the noggin. Doh!",
		bored => "is overcome with boredom. Oh the drudgery!",
		bored => "is terribly bored with target",
		bounce => "bounces up and down",
		bounce => "bounces up and down in front of target",
		bow => "bows down graciously",
		bow => "bows down before target",
		bravo => "applauds. Bravo!",
		bravo => "applauds at target. Bravo!",
		brandish => "brandishes his weapon fiercely",
		brandish => "brandishes his weapon fiercely at target",
		brb => "lets everyone know he'll be right back",
		brb => "lets target know he'll be right back",
		burp => "lets out a loud belch",
		burp => "burps rudely in target's face",
		bye => "waves goodbye to everyone. Farewell!",
		bye => "waves goodbye to target. Farewell!",
		
		
		dance => "bursts into dance",
		dance => "dances with target",
	);


# Convert big list of everything into something more convenient.
my %TARGETED_EMOTES;
my %UNTARGETED_EMOTES;
while (my ($cmd, $response) = each @EMOTES) {
	if ($response =~ /\btarget\b/) {
		$TARGETED_EMOTES{$cmd} = $response;
	} else {
		$UNTARGETED_EMOTES{$cmd} = $response;
	}
}


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	
	if ($address && $said =~ m{^/(?<cmd>me|emote) (?<emote_text>.*?)[.,!?]?$}i) {
		$bot->emote({ who => $who, channel => $channel, body => $+{emote_text} });
		return "NO_RESPONSE";
		
	} elsif ($address && $said =~ m{^/?(?<cmd>\w+) (with |at |to )?(?<target>.*?)[.,!?]?$}i) {
		if (defined $TARGETED_EMOTES{$+{cmd}}) {
			my $target = $+{target} // "";
			my $emote_text = $TARGETED_EMOTES{$+{cmd}} =~ s/\btarget\b/$target/gr;
			$bot->emote({ who => $who, channel => $channel, body => $emote_text });
			return "NO_RESPONSE";
		}
		
	} elsif ($address && $said =~ m{^/?(?<cmd>\w+)$}i) {
		if (defined $UNTARGETED_EMOTES{$+{cmd}}) {
			my $emote_text = $UNTARGETED_EMOTES{$+{cmd}};
			$bot->emote({ who => $who, channel => $channel, body => $emote_text });
			return "NO_RESPONSE";
		}
		
	}
	return;
}


__PACKAGE__->meta->make_immutable;
1;
