#
# Represents a collection of Jeeves plugins; using the Composition pattern, it also behaves *as* a Plugin.
# Calling ->said() on it will call ->said() on each actual plugin in its list, until one returns a defined value.
#
package Jeeves::PluginList;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use Term::ANSIColor;
use Try::Tiny;

use Jeeves::PluginSystem qw/unload_plugin_by_ref/;

use Moose;
extends 'Jeeves::Plugin';


has 'plugins' => (
		is => 'rw',
		isa => 'ArrayRef[Jeeves::Plugin]',
		default => sub { [] },
		traits => ['Array'],
		handles => {
			add => 'push',
		},
	);


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my @plugins = @{$self->plugins()};
	
	foreach my $plugin (@plugins) {
		my $rval = try {
			return $plugin->said($bot, $channel, $who, $address, $said);	# returns from 'try'
		} catch {
			my $pluginname = ref $plugin;	# Not be the nice short name we use, will be e.g. Jeeves::Plugin::KnownDates
			say colored("Uncaught error in $pluginname : $_", 'bold red');
			Jeeves::PluginSystem::unload_plugin_by_ref($plugin);	# To guarantee we unload the correct thing.
			$pluginname =~ s/Jeeves::Plugin:://;
			return "Sorry to be such a bother, but I've caught an unhandled error in the $pluginname plugin. I have unloaded it for now.";
		};
		return if $rval && $rval eq "NO_RESPONSE";
		return $rval if $rval;
	}
	return;
}


sub morning($$)
{
	my ($self, $bot) = @_;
	my @plugins = @{$self->plugins()};
	my @morning_messages;

	foreach my $plugin (@plugins) {
		my @rvals = try {
			$plugin->morning($bot);
		} catch {
			my $pluginname = ref $plugin;	# Not be the nice short name we use, will be e.g. Jeeves::Plugin::KnownDates
			say colored("Uncaught error in $pluginname : $_", 'bold red');
			Jeeves::PluginSystem::unload_plugin_by_ref($plugin);	# To guarantee we unload the correct thing.
			$pluginname =~ s/Jeeves::Plugin:://;
			return "Sorry to be such a bother, but I've caught an unhandled error in the $pluginname plugin. I have unloaded it for now.";
		};
		push @morning_messages, @rvals;
	}
	return @morning_messages;
}


sub every_10m($$)
{
	my ($self, $bot) = @_;
	my @plugins = @{$self->plugins()};
	my @periodic_messages;

	foreach my $plugin (@plugins) {
		my @rvals = try {
			$plugin->every_10m($bot);
		} catch {
			my $pluginname = ref $plugin;	# Not be the nice short name we use, will be e.g. Jeeves::Plugin::KnownDates
			say colored("Uncaught error in $pluginname : $_", 'bold red');
			Jeeves::PluginSystem::unload_plugin_by_ref($plugin);	# To guarantee we unload the correct thing.
			$pluginname =~ s/Jeeves::Plugin:://;
			return "Sorry to be such a bother, but I've caught an unhandled error in the $pluginname plugin. I have unloaded it for now.";
		};
		push @periodic_messages, @rvals;
	}
	return @periodic_messages;
}


__PACKAGE__->meta->make_immutable;
1;

