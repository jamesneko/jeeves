package Jeeves::Command;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use Moose;	# Because I am lazy.

use Jeeves::Memory qw/sir get_pref set_pref list_pref/;
use Jeeves::Substitution;


sub isop($$$)
{
	my ($bot, $channel, $who) = @_;
	my $channel_data = $bot->channel_data($channel);
	return $channel_data->{$who}->{op};
}


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my $sir = sir($who);
	
	if ($address && $said =~ /^eval (?<expression>.*)$/) {
		my $rval = eval {
			return Jeeves::Substitution::safeish_direct_eval($+{expression});
		};
		if ($@) {
			return "I'm afraid I might not be able to comply with your instructions in a timely manner, " . sir($who) . "." if ($@ =~ /GOT_TIRED_OF_WAITING/);
			return "But I don't feel sleepy right now..." if ($@ =~ /'sleep'/);
			return "Haha, nice try, " . sir($who) . "." if ($@ =~ /'system'/);
			return "I don't think so " . sir($who) . "." if ($@ =~ /'exit'/);
			return sir($who) . ", I'm afraid my duties prevent me from dying right now." if ($@ =~ /'die'/);
			return "Have you gone loopy, " . sir($who) . "?" if ($@ =~ /'iteration/);
			return "I'm sorry, I can't do that, " . sir($who) . "." if ($@ =~ /trapped by operation mask/);
			return "Terribly sorry " . sir($who) . ", there was some sort of error while executing your instructions.";
		}
		return $rval;
	
	} elsif ($address && $said =~ /^say (?<message>.*?)(in (?<channel>#\w+))?$/i) {
		$bot->say({ channel => $+{channel} // $channel, body => $+{message}});
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^(notice|notify)( (?<channel>#\w+))? (?<message>.*?)$/i) {
		my $target_channel = $+{channel} // $channel;
		say("Sending notice '$+{message}' to channel $target_channel");
		$bot->notice(channel => $target_channel, body => $+{message});
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^msg (?<who>\w+) (?<message>.*?)$/i) {
		$bot->say({ who => $+{who}, channel => 'msg', body => $+{message}});
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^join (?<channel>#\w+)$/i) {
		$bot->pocoirc->yield('join', $+{channel});
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^(part|leave) (?<channel>#\w+)$/i) {
		$bot->pocoirc->yield('part', $+{channel}, "I have been asked to leave by $who.");
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^op (?<who>\w+?)[.,!]?$/i) {
		if (isop($bot, $channel, $bot->nick)) {
			my $target = $+{who};
			$target = $who if ($target eq 'me');
			$bot->mode("$channel +o $target");
			return "At once, $sir.";
		} else {
			return "Apologies, $sir, I do not have the power to do that.";
		}
	
	} elsif ($address && $said =~ /^prefs\b/i) {
		if ($said =~ /^prefs get (\w+)$/) {
			my $value = get_pref($1);
			if (defined $value) {
				return "The preference $1 is set to '$value', $sir.";
			} else {
				return "There is no preference defined for '$1', $sir.";
			}
			
		} elsif ($said =~ /^prefs set (\w+) (.*)$/) {
			my $oldvalue = get_pref($1);
			return "I'm sorry, $sir, I'm not aware of that preference setting." unless defined $oldvalue;
			set_pref($1, $2);
			return "Very good, $sir, I have set $1 from '$oldvalue' to '$2'.";

		} elsif ($said =~ /^prefs define (\w+) (.*)$/) {
			# secret!
			set_pref($1, $2);
			return "Very good, $sir, I have set $1 to '$2'.";
			
		} elsif ($said =~ /^prefs list/) {
			return "Preferences names: " . join(', ', keys %{ list_pref() });
		
		} else {
			return "Usage: prefs (get <name>|set <name> <value>|list)";
		}
		
	} elsif ($address && $said =~ /^(go away|leave|quit|get lost|begone|get out of here).?$/i) {
		$bot->set_exit_status(0);
		$bot->shut_down_gracefully();
		return "NO_RESPONSE";

	} elsif ($address && $said =~ /^(relog|restart)$/i) {
		$bot->set_exit_status(128);
		$bot->shut_down_gracefully();
		return "NO_RESPONSE";
	}
	return;
}


__PACKAGE__->meta->make_immutable;
1;
