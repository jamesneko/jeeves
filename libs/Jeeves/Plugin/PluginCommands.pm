#
# Jeeves plugin to manage plugins? What?!
#
package Jeeves::Plugin::PluginCommands;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use List::MoreUtils qw/firstval/;

use Jeeves::Memory qw/sir/;
use Jeeves::PluginSystem qw/:all/;

use Moose;
extends 'Jeeves::Plugin';



sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	
	# We only respond to direct queries for this plugin.
	return unless $address;
	
	my $sir = sir($who);
		
	if ($said =~ /^(plugins? list|list plugins?)$/i) {
		my %available_plugins = available_plugins();
		my @loaded_plugins = loaded_plugins();
		my @rval;
		foreach my $pluginname (sort keys %available_plugins) {
			$pluginname = "($pluginname)" unless grep { $_ eq $pluginname } @loaded_plugins;
			push @rval, $pluginname;
		}
		return join(" ", @rval);
	
	} elsif ($said =~ /^(plugins? )?load (?<pluginname>\w+)$/i) {
		my %available_plugins = available_plugins();
		my @loaded_plugins = loaded_plugins();
		my $requestedname = $+{pluginname};
		my $pluginname = firstval { lc($_) eq lc($requestedname) } keys %available_plugins;
		
		return "Sorry $sir, no plugin called '$requestedname' is available." unless $pluginname;
		return "$sir, that plugin is already loaded." if firstval { $_ eq $pluginname } @loaded_plugins;
		
		if (load_plugin($pluginname)) {
			return "Plugin '$pluginname' loaded.";
		} else {
			return "I'm afraid there was an error while attempting to load the plugin. Please check my console for details.\n";
		}

	} elsif ($said =~ /^(plugins? )?reload (?<pluginname>\w+)$/i) {
		my %available_plugins = available_plugins();
		my @loaded_plugins = loaded_plugins();
		my $requestedname = $+{pluginname};
		my $pluginname = firstval { lc($_) eq lc($requestedname) } keys %available_plugins;
		
		return "Sorry $sir, no plugin called '$requestedname' is available." unless $pluginname;
		return "$sir, that plugin is not loaded." unless firstval { $_ eq $pluginname } @loaded_plugins;
		
		if (reload_plugin($pluginname)) {
			return "Plugin '$pluginname' reloaded.";
		} else {
			return "I'm afraid there was an error while attempting to reload the plugin. Please check my console for details.\n";
		}
		
	} elsif ($said =~ /^(plugins? )?unload (?<pluginname>\w+)$/i) {
		my %available_plugins = available_plugins();
		my @loaded_plugins = loaded_plugins();
		my $requestedname = $+{pluginname};
		my $pluginname = firstval { lc($_) eq lc($requestedname) } keys %available_plugins;
		
		return "Sorry $sir, no plugin called '$requestedname' is available." unless $pluginname;
		return "$sir, that plugin is not loaded." unless firstval { $_ eq $pluginname } @loaded_plugins;
		return "No. Stop that." if $pluginname eq 'PluginCommands' && $who eq 'moonburn';
		return "That would not be advisable, $sir." if $pluginname eq 'PluginCommands';
		
		unload_plugin($pluginname);
		return "Plugin '$pluginname' unloaded.";
		
	} elsif ($said =~ /\b(plugins?)\b/i) {
		return "Usage: plugin <list|load|reload|unload> <plugin name>";
	}
}



# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;

