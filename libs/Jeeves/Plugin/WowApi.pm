#
# Jeeves plugin to announce guild levelups.
# https://dev.battle.net/io-docs
#
package Jeeves::Plugin::WowApi;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use JSON::API;
use JSON;
use Try::Tiny;
use Jeeves::Cooldown qw/cooldown resetting_cooldown/;
use Jeeves::Memory qw/sir wow_tokens set_wow_tokens wow_client_id wow_client_secret wow_characters set_wow_characters/;

use Moose;

use WowGuildApi;

extends 'Jeeves::Plugin';


my $wow;


sub save_tokens
{
  my ($tokens) = @_;
  say "Saving tokens: $tokens";
  set_wow_tokens($tokens) unless ref $tokens;
}


sub loaded($$)
{
	my ($self, $pluginname) = @_;
	my @tokens;
	push @tokens, wow_tokens() if wow_tokens();
	$wow = WowGuildApi->new(wow_client_id(), wow_client_secret(), \&save_tokens, @tokens);
	say "WowApi plugin loaded. Saved tokens: " . wow_tokens();
}


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my $sir = sir($who);
	
	if ($address && $said =~ /
	         wow
	         \s+
	         auth
	         /xi) {
		# Ask user to renew auth code
		return "Please visit " . $wow->get_auth_url() . " and then tell me 'wow code <code>'.";

	} elsif ($address && $said =~ /
	         wow
	         \s+
	         code
	         \s+
	         (\w+)
	         /xi) {
		# Set new code for getting tokens
		$wow->set_code($1);
		return "Logged in as: " . $wow->whoami();

	} elsif ($address && $said =~ /
	         wow
	         \s+
	         whoami
	         /xi) {
		# Check current token
		return "Logged in as: " . $wow->whoami();

	} elsif ($address && $said =~ /
	         wow
	         \s+
	         token
	         /xi) {
		# Get current token
		return "Current access token: " . $wow->token_string();

	} elsif ($address && $said =~ /
	         guild
	         \s+
	         levels
	         /xi) {
		# Direct lookup command
		my @changes = $self->fetch_guild_members();
		my $description = $self->describe_guild_member_changes(@changes);
		return $description if $description;

	} elsif ($address && $said =~ /
	         guild
	         \s+
	         info
	         /xi) {
		# Direct lookup command
		my ($json, $res) = $wow->get_guild_roster_legacy();
		say $res->decoded_content;
		return $res->decoded_content;

	} elsif ($address && $said =~ /
	         level
	         \s
	         is
	         \s
	         (\w+)\b
	         /xi) {
		# Lookup character level
		my $charname = ucfirst($1);
		my $cur_characters = wow_characters();
		if ($cur_characters->{$charname}) {
		  return "$charname is level $cur_characters->{$charname}->{level}";
		}
		return "I don't know that person.";

	} elsif ($said =~ /
	         what
	         \s
	         level
	         \s
	         is
	         \s
	         (\w+)\b
	         /xi) {
		# Incidental Lookup character level
		my $charname = ucfirst($1);
		my $cur_characters = wow_characters();
		if ($cur_characters->{$charname}) {
		  return "$charname is level $cur_characters->{$charname}->{level}";
		}
		return "NO_RESPONSE";
		
	}
	return;
}


# formerly run every_10m
sub morning
{
	my ($self, $bot) = @_;
	
	return; #### BROKEN ATM
	
	my @changes = $self->fetch_guild_members();
	my $description = $self->describe_guild_member_changes(@changes);
	return $description if $description;
	return;
}


# Sync with WoW API, store updated member list with level info
sub fetch_guild_members
{
	my ($self, $apikey) = @_;
	say "WowApi: Fetching guild member list.";
	
	my $inc_characters = {};
	try {
		my ($obj, $res) = $wow->get_guild_roster_legacy();
		
		die "WowApi: Received response that was not a HASH: $obj" unless (ref $obj eq 'HASH');
		die "WowApi: Received response that has no 'members' entry: " . JSON->new->encode($obj) unless ($obj->{members});
		die "WowApi: Received response that has a non-array 'members' entry: " . JSON->new->encode($obj) unless (ref $obj->{members} eq 'ARRAY');

		say "Guild data obtained for '$obj->{name}'. Number of members: ", scalar @{$obj->{members}};
		foreach my $member (@{$obj->{members}}) {
			my $char = $member->{character};
			$inc_characters->{$char->{name}} = { name => $char->{name}, level => $char->{level} };
		}
	
	} catch {
		say "Caught exception in WowApi fetch_guild_members: $_";
		return ();
	};
	
	my $cur_characters = wow_characters();
	my @changes = $self->diff_wow_characters($cur_characters, $inc_characters);
	set_wow_characters($inc_characters) if @changes;
	
	return @changes;
}


sub diff_wow_characters
{
	my ($self, $cur_characters, $inc_characters) = @_;
	my @changes;
	foreach my $char (keys %$cur_characters) {
		next unless $inc_characters->{$char};
		if ($inc_characters->{$char}->{level} > $cur_characters->{$char}->{level}) {
			my $level = $inc_characters->{$char}->{level};
			say "$inc_characters->{$char}->{name} is now level $level.";
			push @changes, { name => $inc_characters->{$char}->{name}, level => $level };
		}
	}
	return @changes;
}


# Say stuff that changed.
sub describe_guild_member_changes
{
	my ($self, @changes) = @_;
	my $description;
	if (@changes) {
		my @descs = map { "$_->{name} for reaching level $_->{level}" } @changes;
		
		$description = "Congratulations to ";
		if (@descs == 1) {
			$description .= $descs[0];
		} else {
			my $popped = pop @descs;
			$description .= join(', ', @descs);
			$description .= " and " . $popped;
		}
		$description .= ".";
	}
	return $description;
}



# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;

