#
# Jeeves plugin to look up conversion rates and convert to AUD.
#
package Jeeves::Plugin::Currency;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use charnames qw/:full/;
use Finance::Quote;		# libfinance-quote-perl
use Forex;              # uses openexchangerates.org
use Jeeves::Cooldown qw/cooldown resetting_cooldown/;
use Jeeves::Memory qw/sir currency_codes set_currency_codes openexchangerates_api_key/;
use Jeeves::ListHelper;

use Moose;
extends 'Jeeves::Plugin';


has 'quote' => ( is => 'rw', default => sub { Finance::Quote->new } );

has 'forex' => ( is => 'rw' );
has 'rates' => ( is => 'rw' );
has 'rates_obtained_on' => ( is => 'rw', default => "" );


my $listhelper = Jeeves::ListHelper->new(
		prefix => 'currency',
		list_get_handler => \&currency_codes,
		list_set_handler => \&set_currency_codes,
	);

my $default_currency = 'AUD';
my $USE_FOREX = 1;

sub loaded($$)
{
	my ($self, $pluginname) = @_;
	my $apikey = openexchangerates_api_key();
	$self->forex( Forex->new(APP_ID => $apikey, BASE => 'USD') );
	say "Currency plugin loaded. API key = $apikey";
}


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my $sir = sir($who);
	
	# For currencies mentioned in chat that we should convert without being asked directly.
	my $currencies_rx = join('|', grep { $_ =~ /^\w\w+$/ } currency_codes());
	my $symbols_rx = '(\$|\p{Currency_Symbol})';

	# \N{POUND SIGN}\N{EURO SIGN}\N{CURRENCY SIGN}\N{YEN SIGN}\N{WON SIGN}


	# First check for some basic list management commands: currency <add|remove|list|clear|undo clear> [ABC]
	if (my $command_result = $listhelper->process_commands($bot, $channel, $who, $address, $said)) {
		return $command_result;
	
	} elsif ($address && $said =~ /^currency help/i) {
		return "Usage: currency convert <amount> USD to AUD | currency <add|remove|list|clear|undo clear> <USD|AUD|GBP etc>";
	
	} elsif ($address && $said =~ /
	         ^
	         currency
	         \s+
	         convert
	         \s+
	         (
	           $symbols_rx?(?<amount>[\d\.,-]+) \s+ (?<from>[A-Z]+) \s+ to \s+ (?<to>[A-Z]+)
	         |
	           (?<from>[A-Z]+) \s+ $symbols_rx?(?<amount>[\d\.,-]+) \s+ to \s+ (?<to>[A-Z]+)
	         )
	         /xi) {
		# Direct convert command.
		my $result = $self->convert($+{amount}, $+{from}, $+{to});
		return "I'm uncertain as to how to convert between $+{from} and $+{to}, $sir." unless $result;
		return $self->report($+{amount}, $+{from}, $+{to}, $result) . ".";
	}
	
	# Okay, no direct commands, let's check for mentioned currency values.
	my @responses;
	my $target_currency = $default_currency;
	
	# Oh, the target currency can possibly be modified by mentioning 'in GBP'
	if ($said =~ /\b(in|for|to) (?<currency>[A-Z]{3})\b/) {
		$target_currency = $+{currency};
		say "Plugins::Currency: Setting target currency to '$target_currency'";
	}
	
	# Go through all currencies mentioned on this chat line.
	while ($said =~ /
	         (
	           \b
	           $symbols_rx?(?<amount> [\d\.,-]+  )
	           \s*
	           (?<from> $currencies_rx )
	           \b
	         |
	           \b
	           (?<from> $currencies_rx )
	           (\s|$symbols_rx)*
	           (?<amount> [\d\.,-]+  )
	           \b
	         )
	         /xg) {
		next if $+{from} eq $target_currency;
		# Foreign currency amount mentioned in chat.
		my $result = $self->convert($+{amount}, $+{from}, $target_currency);
		if ( ! $result) {
			return "Ordinarily, $sir, I would convert between $+{from} and $target_currency for you; I seem to be having some difficulty doing so." if cooldown('currency_conversion_broke1', 1800);
			return "Sorry, it's still broken." if cooldown('currency_conversion_broke2', 1800);
			return "NO_RESPONSE";
		}
		
		my $response = $self->report($+{amount}, $+{from}, $target_currency, $result);
		if (resetting_cooldown("convert_$response", 300)) {
			push @responses, $response;
		}
	}
	if (@responses) {
		if (@responses > 1) {
			$responses[-1] = "and " . $responses[-1];
		}
		return join(', ', @responses) . ".";
	}
}



sub convert
{
	my ($self, $amount, $from, $to) = @_;
	say "Plugins::Currency: request for converting '$amount' from '$from' to '$to'.";
	# Sanitise thousands separator. This fails on regions using , as decimals separator.
	# We also fail to recognise spaces or periods for thousands separator. Basically just give me a break ok?
	# We're trying to grok natural text.
	$amount =~ s/,//g;
	my $ratio;
	
	if ($USE_FOREX) {
		$self->update_rates();
		my $USD_to_from = $self->rates()->{rates}->{$from};
		say "No currency code for $from returned by openexhangerates.org!" unless defined $USD_to_from;
		my $USD_to_to = $self->rates()->{rates}->{$to};
		say "No currency code for $to returned by openexhangerates.org!" unless defined $USD_to_to;
		
		my $from_to_USD = 1.0 / $USD_to_from;
		$ratio = $from_to_USD * $USD_to_to;
	} else {
		$ratio = $self->quote()->currency($from, $to);
	}
	return unless $ratio;
	
	say "Plugins::Currency: ratio is $ratio";
	my $converted = $amount * $ratio;
	return $converted;
}


sub update_rates
{
	my ($self) = @_;
	
	say "update_rates: Rates last obtained on ", $self->rates_obtained_on();

	# Look up openexchangerates.org rates only once per day.
	my $date_today = sprintf("%4.4d-%2.2d-%2.2d", (localtime())[5]+1900, (localtime())[4]+1, (localtime())[3]);
	return if $self->rates_obtained_on() eq $date_today;
	
	say "Downloading new rates from openexchangerates.org...";
	$self->rates( $self->forex()->get_rates() );
	if ( ! defined $self->rates()) {
		say "No rates downloaded, shit";
		return;
	}
	if ( ! defined $self->rates()->{rates}) {
		say "No {rates} defined after getting data, shit";
		return;
	}

	$self->rates_obtained_on($date_today);
}


sub report
{
	my ($self, $amount, $from, $to, $result) = @_;
	$result = sprintf("%.02f", $result);
	return "$amount $from is $result $to";
}


# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;

