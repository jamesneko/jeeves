#
# Jeeves plugin to look up share prices.
#
package Jeeves::Plugin::SharePrice;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use charnames qw/:full/;
use Finance::Quote;		# libfinance-quote-perl
use Jeeves::Cooldown qw/cooldown resetting_cooldown/;
use Jeeves::Memory qw/sir/;

use Moose;
extends 'Jeeves::Plugin';


has 'quote' => ( is => 'rw', default => sub { Finance::Quote->new } );


my $default_currency = 'AUD';
my $default_exchange = 'nasdaq';

sub said($$$$$$)
{
	#### DISABLED While Yahoo! Finance! API! is gone...
	return;
	
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my $sir = sir($who);
	
	if ($address && $said =~ /
	         (share|stock)
	         \s*
	         price
	         (\s*for)?
	         \s+
	         ((?<exchange> \w+) \s* : \s*)?
	          (?<code>     \w+)
	         /xi) {
		# Direct lookup command: shareprice nasdaq:TEAM or stock price nasdaq:TEAM or just share price TEAM.
		my $exchange = $+{exchange} // $default_exchange;
		my $code     = $+{code};
		my $result   = $self->lookup($exchange, $code);
		if ( ! $result) {
			return "Apologies, there was some sort of problem looking up the share price of $exchange:$code." if cooldown('share_lookup_broke1', 1800);;
			return "Sorry, it's still broken." if cooldown('share_lookup_broke2', 1800);
			return "NO_RESPONSE";
		}
		return $self->report($exchange, $code, $result) . ".";
		
	} elsif ($said =~ /
	         \b
	         (?<exchange> (nasdaq|nyse|australia|europe|canada|usa)) :
	         (?<code>     \w+)
	         \b
	         /xi) {
		# Indirect lookup command: mention an exchange name with e.g. nasdaq:<code>.
		my $exchange = $+{exchange} // $default_exchange;
		my $code     = $+{code};
		my $result   = $self->lookup($exchange, $code);
		if ( ! $result) {
			return "Apologies, there was some sort of problem looking up the share price of $exchange:$code." if cooldown('share_lookup_broke1', 1800);;
			return "Sorry, it's still broken." if cooldown('share_lookup_broke2', 1800);
			return "NO_RESPONSE";
		}
		return $self->report($exchange, $code, $result) . ".";
		
	}
	return;
}



sub lookup
{
	my ($self, $exchange, $code) = @_;
	say "Plugins::SharePrice: request for share price of '$exchange:$code'.";
	
	my %info = $self->quote()->fetch($exchange, $code);
	return unless %info;
	return \%info;
}


sub report
{
	my ($self, $exchange, $code, $result) = @_;
	
	# Get value
	my $usd = $result->{$code, "price"} // "???";
	
	# Convert currency
	my $from  = $result->{$code, "currency"};
	my $to    = $default_currency;
	my $ratio = $self->quote()->currency($from, $to);
	my $aud = "???";
	$aud = $usd * $ratio if $ratio;
	$aud = sprintf("%.02f", $aud);
	
	return "$exchange:$code is at $usd $from ($aud $to)";
}


# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;

