#
# Jeeves plugin to convert times between timezones.
#
package Jeeves::Plugin::TimeZone;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use DateTime;
use TryCatch;	# DateTime can die if you give it e.g. an invalid hour. Thanks moonburn!

use Moose;
extends 'Jeeves::Plugin';




my %offset_abbreviations = (
		PST => "-0800",	# Please send tell.
		PDT => "-0700",
		EST => "-0500",	# Americans.
		EDT => "-0400",
		CDT => "-0500",
		CST => "-0600",
		CET => "+0100",	# Europeans.
		CEST => "+0200",
		AEST => "+1000",	# lol, I guess Australians too? At least it's got an 'A' to attempt to distinguish it...
		AEDT => "+1100",	# stupid daylight savings
		ACST => "+0930",
		ACDT => "+1030",
		ASWT => "+0800",
		Azeroth => "America/Los_Angeles",	# Can't use regexp here sadly, since we need to also lookup what matched.
		azeroth => "America/Los_Angeles",
		'US Pacific' => "America/Los_Angeles",
		'US pacific' => "America/Los_Angeles",
		'Pacific' => "America/Los_Angeles",
		'pacific' => "America/Los_Angeles",
		'US Eastern' => "America/New_York",
		'US eastern' => "America/New_York",
		'Eastern' => "America/New_York",
		'eastern' => "America/New_York",
		'US Central' => "America/Chicago",
		'US central' => "America/Chicago",
		'Central' => "America/Chicago",
		'central' => "America/Chicago",
		GMT => "UTC",		# I am really surprised this was not in the main list.
	);
my $zones_rx = join('|', DateTime::TimeZone->all_names(), keys %offset_abbreviations);
$zones_rx = qr/$zones_rx|([+-]\d{4})/;
my $local_tz = DateTime::TimeZone->new(name => 'Australia/Sydney');


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	
	# Be able to list the official timezone names.
	if ($address && $said =~ /\btimezone grep (.*)$/) {
		my $pattern = $1;
		my @matches = grep { $_ =~ /$pattern/i } DateTime::TimeZone->all_names();
		if (@matches) {
			return "The official timezones I'm aware of are: ", join(', ', @matches);
		} else {
			return "I couldn't find any matching timezone names, sorry.";
		}
	}
	
	# Allow people to ask Jeeves for times in non-Australia/Sydney zones.
	my $compute_tz = $local_tz;
	if ($said =~
			m{
				\b
				(for|in)
				\s+
				(?<zone>    $zones_rx )
				\b
			}xi) {
		# Map stupid non-standard abbreviations to UTC offsets.
		my $zone = $offset_abbreviations{$+{zone}} // $+{zone};
		$compute_tz = DateTime::TimeZone->new(name => $zone);
	}
	
	my @responses;
	while ($said =~
			m{
				(?<match>
					(?<!:)							# There is no colon i.e. we're not looking at a big hh:mm:ss timestamp.
					\b
					(?<hour>    \d+       )
					(
						:
						(?<min>  \d+       )
					)?
					\s*
					(?<ampm>    am|pm|a\.m\.|p\.m\.     )?
					\s+
					(?<zone>    $zones_rx )		# specifying timezone (as abbreviation or as e.g. Australia/Sydney or UTC offset -0800) is mandatory.
					\b
				)
			}xgi) {
		# Map stupid non-standard abbreviations to UTC offsets.
		my $zone = $offset_abbreviations{$+{zone}} // $+{zone};
		
		try {
			# Set up a DateTime object assuming today, with the given hour/minute and zone.
			my $datetime = DateTime->today(time_zone => $zone);
			my ($hour, $min) = ($+{hour}, $+{min});
			$hour = 0 if uc($+{ampm} // "") eq 'AM' && $hour == 12;		# 12 am refers to 00:00
			$hour += 12 if uc($+{ampm} // "") eq 'PM' && $hour != 12;	# 1 pm refers to 13:00, but not 12 pm.
			$min //= 0;
			$datetime->set_hour($hour);
			$datetime->set_minute($min);
			
			# Convert that into our local timezone.
			my $converted = $datetime->clone()->set_time_zone($compute_tz);
			
			my $the_next_day = "";
			my $delta = $converted->day_of_year() - $datetime->day_of_year();
			$the_next_day = " the following day" if ($delta == 1);
			$the_next_day = " the previous day" if ($delta == -1);
			# okay, just for that one marginal case...
			my $delta_year = $converted->year() - $datetime->year();
			$the_next_day = " the following year" if ($delta_year == 1);
			$the_next_day = " the previous year" if ($delta_year == -1);
			
			# Ignore the identity conversion.
			next if ($delta == 0 && $delta_year == 0 && $converted->strftime("%I:%M %p") eq $datetime->strftime("%I:%M %p"));
			
			# Add to our response.
			push @responses, "'$+{match}' is " . $converted->strftime("%I:%M %p") . $the_next_day;
		} catch ($err) {
			warn "Caught an error when attempting to manipulate DateTime: $err\n";
		}
	}
	if (@responses) {
		return "For " . $compute_tz->name() . ", " . join(", ", @responses) . ".";
	}
	return;
}


# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;
