#
# Jeeves plugin to remember birthdays etc.
#
package Jeeves::Plugin::KnownDates;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use DateTime;
use List::MoreUtils qw/firstidx uniq/;
use Jeeves::Memory qw/sir canonical_name known_dates set_known_dates cookie/;

use Moose;
extends 'Jeeves::Plugin';

# So, when we inevitably rewrite this, a good approach might be to just loop through all dateids stored and
# convert them into DateTime objects for the upcoming year(s). Then we could do more operations on them.
# But for now, it'll suffice to just include a hack for Monday#2 June style dateids that work for checking
# against 'today' etc. but aren't as easy to inquire about.


my $literalhash = qr/\#/;
my @DAY_NAMES = qw/Monday Tuesday Wednesday Thursday Friday Saturday Sunday/;
my @MONTH_NAMES = qw/January Febuary March April May June July August September October November December/;
my @MONTH_ABBREVS = qw/Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec/;
my $dayofweek_rx = qr/(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)/i;
my $months_rx = qr/(January|February|March|April|May|June|July|August|September|October|November|December|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)/i;
my $daynum_rx = qr{ (the\s)? \d{1,2} (st|nd|rd|th)? }xi;
my $date_rx = qr{
		\b
		(
			$daynum_rx \s+ of \s+ $months_rx		# (the) 21st of May
		|
			$months_rx \s+ $daynum_rx				# May (the) 21st
		|
			yesterday
		|
			today
		|
			tomorrow
		|
			$daynum_rx \s+ $dayofweek_rx \s+ (in|of) \s+ $months_rx		# (the) 2nd Monday in June
		|
			$dayofweek_rx $literalhash \d+ \s+ $months_rx					# Monday#2 June
		)
		\b
	}xi;


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my $sir = sir($who);
	
	if ($said =~ /^PARSEDAY \s* (?<date>$date_rx)?$/x && $address) {
		warn "PARSEDAY triggered\n";
		return parse_date($+{date} // "today");
		
	} elsif ($said =~ /
	         ^
	         What \s+ day \s+ is
	         \s+
	         (
	         	(it|it \s+ (?<date>$date_rx))
	         |
	         	(?<date>$date_rx)
	         )
	         \s*
	         \?*
	         $/xi) {
		# Respond to inquiries about the current day, if we know.	
		my $datestr = what_day_is_it($+{date} // "today");	# A formatted list of datenames.
		return $datestr if $datestr;
		return "I don't know of any special occasion, $sir." if $address;
		return "NO_RESPONSE";
		
	} elsif ($said =~ /^(When|What day) is (?<datename>.*?)\??$/i) {
		# Respond to inquiries about what date a given known_day is, if we know.
		my $datename = $+{datename};
		my $dateid = when_is_day($datename);	# Turns Nekoday into "21 May"
		return "$datename is $dateid." if $dateid;
		return "I don't know of that occasion, $sir." if $address;
		return "NO_RESPONSE";
		
	} elsif ($address && $said =~ m{
		# Be able to learn new dates.
				^
				(
					(?<date>		$date_rx )	# e.g. the 21st of May, May 21st, today
					\s+
					(is|was|will be)
					\s*
					(?<datename> .*? )			# e.g. Nekoday, Neko's Birthday
				|
					(?<datename> .*? )			# e.g. Nekoday, Neko's Birthday
					\s+
					(is|was|will be)
					\s*
					(?<date>		$date_rx )	# e.g. the 21st of May, May 21st, today, the 2nd Monday in June
				)
				\s*
				\.?
				$
			}xi) {
		warn "learn new day triggered\n";

		# Turn the date into something standard.
		my $dateid = parse_date($+{date});
		
		my $datename = $+{datename};
		# Basic Post-processing on the day name so people can say "today is my birthday".
		$datename =~ s/\bmy\b/${who}'s/i;
		
		remember_day($dateid, $datename);
		
		return qq{Thank you $sir. $dateid is "$datename"; I shall make a note of it.};
	}
	
	return;
}


sub morning
{
	return what_day_is_it("today");
}


# Takes natural language date spec, returns list of datenames that fall on that day.
sub what_day_is_it
{
	my ($datequery) = @_;
	my $dateid = parse_date($datequery);
	
	# This will look up anything we know by its CANONICAL dateid, where stuff like 'today' has already been
	# converted to a "21 May" and stuff directly specified as 'the 2nd Monday in June' has been converted to
	# "Monday#2 June".
	my $dates = known_dates($dateid);
	my @datelist = @$dates;
	
	# There is, however, a special case where we want 'today' to resolve to whatever today's "Monday#2 June"
	# equivalent should be, because we want to be able to congratulate Her Majesty properly.
	push @datelist, @{known_dates(parse_date_as_dayofweeknum($datequery))} if $datequery eq 'today';
	
	@datelist = uniq @datelist;
	return if (@datelist == 0);
	
	my $datestr = ucfirst($datequery) . " is ";
	if (@datelist == 1) {
		$datestr .= $datelist[0];
	} else {
		my $popped = pop @datelist;
		$datestr .= join(', ', @datelist);
		$datestr .= " and " . $popped;
	}
	$datestr .= ".";
	return $datestr;
}


# For example, when is Wobinday? Returns dateid.
sub when_is_day
{
	my ($datename) = @_;
	foreach my $dateid (keys %{cookie('known_dates')}) {
		my $dates = known_dates($dateid);
		foreach my $known_date (@$dates) {
			if (lc($datename) eq lc($known_date)) {
				return $dateid;
			}
		}
	}
	return;
}


sub remember_day($$)
{
	my ($dateid, $dayname) = @_;
	my $dates = known_dates($dateid) // [];
	push @$dates, $dayname;
	set_known_dates($dateid, $dates);
}


# Given a user date string that has presumably matched our regexps, extract the relevant
# bits and normalise it to the form "DD Monthname".
# OR: "Dayname#D Monthname".
sub parse_date($)
{
	my ($userdate) = @_;
	my $datetime = DateTime->today(time_zone => 'Australia/Sydney');
	my $today = $datetime->strftime("%d %B");
	$datetime->add( days => 1 );
	my $tomorrow = $datetime->strftime("%d %B");
	$datetime->subtract( days => 2 );
	my $yesterday = $datetime->strftime("%d %B");
	
	# process relative dates first
	$userdate =~ s/^yesterday$/$yesterday/i;
	$userdate =~ s/^today$/$today/i;
	$userdate =~ s/^tomorrow$/$tomorrow/i;
	
	if ($userdate =~ m/(?<dayofweek>$dayofweek_rx)/) {
		# SPECIAL CASE: It looks like "the 2nd Monday in June" style user date spec.

		# Extract normalised dayofweek
		my $dayofweek;
		if ($userdate =~ m/(?<dayofweek>$dayofweek_rx)/) {
			$dayofweek = ucfirst lc $+{dayofweek};
		}
		
		# Extract dayofweeknum
		my $dayofweeknum;
		if ($userdate =~ m/(\d+)/) {
			$dayofweeknum = sprintf("%01d", $1);
		}
	
		# Extract normalised month
		my $month;
		if ($userdate =~ m/(?<month>$months_rx)/) {
			$month = $+{month};
			
			my $idx = firstidx { $_ eq $month } @MONTH_ABBREVS;
			if ($idx >= 0) {
				$month = $MONTH_NAMES[$idx];
			}
		}

		# Reformat.
		if (defined $dayofweek && defined $dayofweeknum && defined $month) {
			$userdate = "$dayofweek#$dayofweeknum $month";
		}

	} else {
		# NORMAL CASE: 4th of June, June the 4th, etc

		# Extract daynum
		my $daynum;
		if ($userdate =~ m/(\d+)/) {
			$daynum = sprintf("%02d", $1);
		}
		
		# Extract normalised month
		my $month;
		if ($userdate =~ m/(?<month>$months_rx)/) {
			$month = $+{month};
			
			my $idx = firstidx { $_ eq $month } @MONTH_ABBREVS;
			if ($idx >= 0) {
				$month = $MONTH_NAMES[$idx];
			}
		}
	
		# Reformat.
		if (defined $daynum && defined $month) {
			$userdate = "$daynum $month";
		}
	}
	
	return $userdate;
}


# Parses a date like 'today', 'yesterday', 'tomorrow' and returns a dateid like Monday#2 June.
# Returns original string on any input it can't convert easily into such a dateid.
sub parse_date_as_dayofweeknum($)
{
	my ($userdate) = @_;
	return unless $userdate eq 'today';		# It's super limited and I don't care!
	my $dt = DateTime->today(time_zone => 'Australia/Sydney');

	my $day_of_month = $dt->day();		# 1..31
	my $day_name = $dt->day_name();		# Monday
	my $month_name = $dt->month_name();	# June
	
	# If $day_of_month is 1..6, we are definitely the first $day_name of the month.
	# After that, we are definitely the second, and so on.
	my $day_of_week_num = int(($day_of_month - 1) / 7) + 1;
	
	return "$day_name#$day_of_week_num $month_name";
}


# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;
