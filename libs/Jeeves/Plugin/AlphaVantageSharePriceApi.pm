#
# Jeeves plugin to look up (usaland) share prices.
#
package Jeeves::Plugin::AlphaVantageSharePriceApi;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;
use charnames qw/:full/;

use JSON::API;
use Jeeves::Cooldown qw/cooldown resetting_cooldown/;
use Jeeves::Memory qw/sir alphavantage_api_key/;
use Jeeves::PluginSystem qw/get_plugin_by_name/;

use Moose;
use Number::Format;
extends 'Jeeves::Plugin';


my $default_currency = 'AUD';
my $default_exchange = 'nasdaq';
my $number_format = Number::Format->new();

sub loaded($$)
{
	my ($self, $pluginname) = @_;
	my $apikey = alphavantage_api_key();
	say "AlphaVantageSharePriceApi plugin loaded. API key = $apikey";
}


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	my $sir = sir($who);
	
	if ($address && $said =~ /
	         (share|stock)
	         \s*
	         price
	         (\s*for|\s*of)?
	         \s+
	         ((?<exchange> \w+) \s* : \s*)?
	          (?<code>     \w+)
	         /xi) {
		# Direct lookup command: shareprice nasdaq:TEAM or stock price nasdaq:TEAM or just share price TEAM.
		my $exchange = $+{exchange} // $default_exchange;
		my $code     = $+{code};
		my $result   = $self->lookup($exchange, $code);
		if ( ! $result) {
			return "Apologies, there was some sort of problem looking up the share price of $exchange:$code." if cooldown('share_lookup_broke1', 1800);;
			return "Sorry, it's still broken." if cooldown('share_lookup_broke2', 1800);
			return "NO_RESPONSE";
		}
		return $self->report($exchange, $code, $result) . ".";
		
	} elsif ($said =~ /
	         \b
	         (?<exchange> (nasdaq|nyse|usa)) :
	         (?<code>     \w+)
	         \b
	         /xi) {
		# Indirect lookup command: mention an exchange name with e.g. nasdaq:<code>.
		my $exchange = $+{exchange} // $default_exchange;
		my $code     = $+{code};
		my $result   = $self->lookup($exchange, $code);
		if ( ! $result) {
			return "Apologies, there was some sort of problem looking up the share price of $exchange:$code." if cooldown('share_lookup_broke1', 1800);;
			return "Sorry, it's still broken." if cooldown('share_lookup_broke2', 1800);
			return "NO_RESPONSE";
		}
		return $self->report($exchange, $code, $result) . ".";
		
	}
	return;
}



sub lookup
{
	my ($self, $exchange, $code) = @_;
	say "Plugins::AlphaVantageSharePriceApi: request for share price of '$exchange:$code'.";
	
	my $api = JSON::API->new("https://www.alphavantage.co");
	my $obj = $api->get("query", {
			function   => 'TIME_SERIES_DAILY',
			outputsize => 'compact',
			datatype   => 'json',
			symbol     => $code,
			apikey     => alphavantage_api_key(),
		});
	my @days = sort keys %{$obj->{"Time Series (Daily)"}};
	
	say "Latest result obtained for ", $days[-1];
	my $today = $obj->{"Time Series (Daily)"}->{$days[-1]};
	my $yesterday = $obj->{"Time Series (Daily)"}->{$days[-2]};
	$today->{volume_delta} = $today->{"5. volume"} - ($yesterday->{"5. volume"} // 0);
	return $today;
	
	#$HASH1 = {
	#"1. open"   => '52.2900',
	#"2. high"   => '52.8200',
	#"3. low"    => '51.9200',
	#"4. close"  => '52.4000',
	#"5. volume" => 634345
	#};
}


sub report
{
	my ($self, $exchange, $code, $result) = @_;
	
	# Get value
	my $usd = $result->{"4. close"};
	my $from = "USD";
	return "$exchange:$code is at um... I have no idea" unless $usd;
	
	my $delta = $result->{"4. close"} - $result->{"1. open"};
	my $deltastring = $self->format_delta($delta, "%02.2f");
	
	my $volume = $number_format->format_number($result->{"5. volume"})
	           . " (" . $self->format_delta($result->{volume_delta}, "NF") . ")";
	
	# Convert currency
	my $currency_plugin = get_plugin_by_name("Currency");
	if ($currency_plugin) {
		say "Obtained Currency plugin: $currency_plugin";
		my $to = $default_currency;
		my $aud = $currency_plugin->convert($usd, $from, $to);
		
		if ($aud) {
			$usd = sprintf("%02.2f", $usd);
			$aud = sprintf("%02.2f", $aud);
			return "$exchange:$code is at $usd $from $deltastring ($aud $to) vol $volume";
		} else {
			$usd = sprintf("%02.2f", $usd);
			return "$exchange:$code is at $usd $from $deltastring vol $volume";
		}
	} else {
		$usd = sprintf("%02.2f", $usd);
		return "$exchange:$code is at $usd $from $deltastring";
	}
}


sub format_delta
{
	my ($self, $delta, $sprintf_fmt) = @_;
	my $delta_arrow = $delta;
	if ($sprintf_fmt eq "NF") {
		$delta = $number_format->format_number($delta);
		$sprintf_fmt = "%s";
	}
	return sprintf( $self->delta_arrow($delta_arrow) . " $sprintf_fmt", $delta);
}


sub delta_arrow
{
	my ($self, $delta) = @_;
	# https://github.com/myano/jenni/wiki/IRC-String-Formatting
	my $colour = "\x03";  # ctrl-C
	my $bold   = "\x02";
	my $reset  = "\x0F";
	
	if ($delta < 0) {
		return "$bold${colour}04\N{DOWNWARDS ARROW}$reset";
	} else {
		return "$bold${colour}03\N{UPWARDS ARROW}$reset";
	}
}


# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;

