#
# Jeeves plugin to look up titles of URLs mentioned in chat.
#
package Jeeves::Plugin::UrlTitle;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use Encode qw/decode_utf8/;
use 5.018;

use URI;
use URI::Title;		# liburi-title-perl
use Jeeves::Cooldown qw/cooldown resetting_cooldown/;
use Jeeves::Memory qw/sir urltitle_hostnames set_urltitle_hostnames/;
use Jeeves::ListHelper;

use Moose;
extends 'Jeeves::Plugin';


my $listhelper = Jeeves::ListHelper->new(
		prefix => 'urltitle',
		list_get_handler => \&urltitle_hostnames,
		list_set_handler => \&set_urltitle_hostnames,
	);


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	
	# First check for some basic list management commands: urltitle <add|remove|list|clear|undo clear> [hostname]
	if (my $command_result = $listhelper->process_commands($bot, $channel, $who, $address, $said)) {
		return $command_result;
	
	} elsif ($address && $said =~ /^urltitle help/i) {
		return "Usage: urltitle <add|remove|list|clear|undo clear> hostname";
	
	} elsif ($said =~ m!\b(?<url>https?://\S+)!) {
		my $url = $self->url_hacks($+{url});
		# Saw a URL!
		
		# Is it one we should care about?
		return unless $self->url_matches(URI->new($url));
		
		# Are we in a loop or spamming?
		if ( ! resetting_cooldown("process_url_title", 2)) {
			say "$who mentioned URL $url in chat, but we are running too frequently.";
			return;
		}
		if ( ! cooldown("process_url_title:$url", 10)) {
			say "$who mentioned URL $url in chat, but we did that one already.";
			return;
		}

		# Get it!
		my $title;
		if ($url =~ m!/((www\.)?youtube\.com|youtu.be)/!) {
			say "$who mentioned URL $url in chat, looking it up via youtube-dl...";
			$title = $self->get_youtube_title($url);
		} else {
			say "$who mentioned URL $url in chat, looking it up via title tag...";
			$title = URI::Title::title($url);
		}
		unless ($title) {
			say "lookup of URL failed.\n";
			return;
		}
		
		# Tell people!
		say "URL title is '$title'.";
		$title = $self->title_hacks($url, $title);
		
		return if $title eq '';
		return "｢$title｣";
	}
}


sub title_hacks($$$)
{
	my ($self, $url, $title) = @_;
	# Fix up imgur titles if they're the default untitled title.
	if ($url =~ m!imgur.com/!i) {
		$title =~ s!Imgur: The most awesome images on the Internet!!;
		$title =~ s!Imgur: The magic of the Internet!!;
	}
	# neaten up twitter titles a bit.
	if ($url =~ m!twitter.com/!i) {
		$title =~ s!^twitter - !!;
	}
	return $title;
}


sub url_hacks($$)
{
	my ($self, $url) = @_;
	# Fix up imgur urls to ask for the HTML instead of the png
	if ($url =~ m!imgur.com/!i) {
		$url =~ s!\.(png|jpg|jpeg|gif|gifv|webm)$!!;
	}
	return $url;
}


sub url_matches($$)
{
	my ($self, $uri) = @_;
	my $host = $uri->host();
	foreach my $testhostname (urltitle_hostnames()) {
		# a given url from m.youtube.com should match if we allow youtube.com
		return 1 if $host =~ m/\b${testhostname}$/i;
	}
}


sub get_youtube_title
{
  my ($self, $url) = @_;
  my $quoted_url = quotemeta($url);
  my $title = decode_utf8(`youtube-dl --get-title $quoted_url`);
  chomp $title;
  return $title;
}


# Actually, we don't want this for Reasons relating to Class::Load # __PACKAGE__->meta->make_immutable;
1;

