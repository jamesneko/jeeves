package Jeeves::Substitution;

# We aren't using this anymore, people hate it.

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use Moose;					# Because I am lazy.
use Term::ANSIColor;		# Because Pretty.
use Safe;					# Because we are EVALING USER-SUPPLIED STRINGS OMG AAAAAH

use Jeeves::Memory qw/sir get_pref get_pref_is_true/;


has 'max_said_recently' => ( is => 'rw', isa => 'Num', default => 20 );
has 'said_recently' => (
		is => 'rw',
		isa => 'ArrayRef[Str]',
		default => sub { [] },
		traits => ['Array'],
		handles => {
				unshift_said_recently => 'unshift',
				pop_said_recently => 'pop',
				count_said_recently => 'count',
				list_said_recently => 'elements',
			},
	);


sub said($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;
	
	# Only concern ourselves with things said in public chat that don't address us directly.
	return if $channel eq 'msg';
	return if $address;
	
	# It can be turned off.
	return unless get_pref_is_true('substitutions');
	
	# A lot of people use a quick shorthand s/thing/thing form that omits the trailing delimiter.
	# Correct it. We can't easily support backslashes with this form either.
	if ($said =~ m{
						^s
						(?<delimiter> /)
						(?<match> .+?)
						\g{delimiter}
						(?<substitution> [^/]*?)
						$}x
					&& get_pref_is_true('substitutions_shorthand')
						) {
		if ($said !~ m{/[msixpodualgcer]*;?$}) {
			say "Correcting '$said' to '$said/;'.";
			$said .= '/;';
		}
	}
	# If someone uses a s///; command, apply it.
	if ($said =~ m{
						^s
						(?<delimiter> [/'"`!^])
						(?<match> .+?)
						(?<!\\)\g{delimiter}
						(?<substitution> .*?)
						(?<!\\)\g{delimiter}
						(?<modifiers> [msixpodualgcer]*)
						;?
						$}x) {
		# We got what looked like a match!
		my $match = $+{match} or return;
		my $substitution = $+{substitution} or return;
		my $modifiers = $+{modifiers} // "";
		
		say "It looks like you want me to substitute '$match' with '$substitution', with modifiers '$modifiers'.";
		
		# Don't ever use the /e or /ee modifier.
####		return "Haha, nice try." if $modifiers =~ /e/;
		my $expression = $said;
		
		# Search through our recent history (early entries are most recent), applying the substitution.
		# If it sticks to any, assume that's what we should have been applying it to.
		my $modified = eval {
			
#			## First, let's set up a timeout so we don't get stuck forever on some troll's looping s///e or whatever.
#			#$SIG{ALRM} = sub { die "GOT_TIRED_OF_WAITING\n" };
#			#alarm(3);
		
			# Actually do the iteration over recent history.
			foreach my $recent ($self->list_said_recently()) {
				# Build and wrap the substitution up in an eval guarded by the Safe module.
				#my $modified = safeish_substitution($match, $substitution, $modifiers, $recent);
				my $modified = safeish_substitution_eval($expression, $recent);
				if ($modified) {
#					## Found it! Cancel the alarm and return out of this eval.
#					#alarm(0);
					# Allow s///; to work on a previously returned substitution, even though it's Jeeves saying it.
					$self->unshift_said_recently($modified);
					return $modified;
				}
			}

#			## Didn't find anything, but cancel the alarm before we leave the eval!
#			#alarm(0);
			
		};
#		alarm(0);
		if ($@) {
			return "I'm afraid I might not be able to comply with your instructions in a timely manner, " . sir($who) . "." if ($@ =~ /GOT_TIRED_OF_WAITING/);
			return "But I don't feel sleepy right now..." if ($@ =~ /'sleep'/);
			return "Haha, nice try, " . sir($who) . "." if ($@ =~ /'system'/);
			return "I don't think so " . sir($who) . "." if ($@ =~ /'exit'/);
			return sir($who) . ", I'm afraid my duties prevent me from dying right now." if ($@ =~ /'die'/);
			return "Have you gone loopy, " . sir($who) . "?" if ($@ =~ /'iteration/);
			return "I'm sorry, I can't do that, " . sir($who) . "." if ($@ =~ /trapped by operation mask/);
			return "Terribly sorry " . sir($who) . ", there was some sort of error while executing your instructions.";
		}

		return $modified if $modified;
		return "Terribly sorry " . sir($who) . ", I can't seem to identify the line to which I should be applying your substitution.";
		
	} else {
		# If it's not an s///; command but it is something someone said in public chat, remember that.
		return if ($said =~ m{^s/});		# Unless it's a disabled shorthand s//.
		$self->unshift_said_recently($said);
		$self->pop_said_recently() if ($self->count_said_recently() > $self->max_said_recently());
	}
	return;
}


# Do a substitution using eval in the safest way possible.
sub safeish_substitution($$$$)
{
	my ($match, $substitution, $modifiers, $text) = @_;
	say colored("safeish_substitution(", 'bold yellow')
			. colored("$match, $substitution, $modifiers, $text", 'yellow')
			. colored(") called", 'bold yellow');
	
	my $expression = "s/${match}/${substitution}/${modifiers};";
   return safeish_substitution_eval($expression, $text);
}

sub safeish_substitution_eval($$)
{
	my ($expression, $text) = @_;
	my $rtext;
	my $matches;
	my $safe = Safe->new();

	# Permit all of :base_core only, including leaveeval needed by Safe.
	$safe->permit_only(qw/:base_core/);
	# But not these from :base_core:-
	$safe->deny(qw/warn die entersub leavesub leavesublv return method method_named/);
	
	# Permit all of :base_mem; this opens us to a resource attack but we really do need to be able to concat strings and things.
	$safe->permit(qw/:base_mem/);
	
#	#TESTING
#	$safe->permit(qw/:base_loop/);

	# Also permit a small selection from :base_orig, because there's a lot of crap there apparently needed by Safe's preamble.
	# And we want subst and regexp related stuff, probably.
	$safe->permit(qw/
			gvsv gv gelem
			padsv padav padhv padcv padany padrange introcv clonecv
			once
			rv2gv refgen srefgen ref
			pushre regcmaybe regcreset regcomp subst substcont
		/);
	
	say colored("EVAL: $expression", 'red');
	
	{
		local $@;
		$_ = $text;
		$matches = $safe->reval( $expression );
		$rtext = $_;
		if ($@) {
			say colored("EVAL FAILED: $@", 'bold red');
			die $@;
		}
	}

	if ($matches) {
		return $rtext;
	} else {
		return;
	}
}


sub safeish_direct_eval($)
{
	my ($expression) = @_;
	my $rtext;
	my $safe = Safe->new();

	# Permit all of :base_core only, including leaveeval needed by Safe.
	$safe->permit_only(qw/:base_core/);
	# But not these from :base_core:-
	$safe->deny(qw/warn die entersub leavesub leavesublv return method method_named/);
	
	# Permit all of :base_mem; this opens us to a resource attack but we really do need to be able to concat strings and things.
	$safe->permit(qw/:base_mem/);
	
#	#TESTING
#	$safe->permit(qw/:base_loop/);

	# Also permit a small selection from :base_orig, because there's a lot of crap there apparently needed by Safe's preamble.
	# And we want subst and regexp related stuff, probably.
	$safe->permit(qw/
			gvsv gv gelem
			padsv padav padhv padcv padany padrange introcv clonecv
			once
			rv2gv refgen srefgen ref
			pushre regcmaybe regcreset regcomp subst substcont
		/);
	
	# For direct evals, I guess we could also allow stuff like rand()
	$safe->permit(qw/
			rand
		/);
	
	say colored("EVAL: $expression", 'red');
	
	{
		local $@;
		$rtext = $safe->reval( $expression );
		if ($@) {
			say colored("EVAL FAILED: $@", 'bold red');
			die $@;
		}
	}

	if (defined $rtext && $rtext ne "") {
		return $rtext;
	} else {
		return;
	}
}



__PACKAGE__->meta->make_immutable;
1;
