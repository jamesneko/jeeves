#
# For other plugins to use to help with mundane list management commands.
#
package Jeeves::ListHelper;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use List::MoreUtils qw/firstidx/;
use Jeeves::Memory qw/sir/;

use Moose;

# The prefix used to differentiate this list add/del stuff from other lists; typically the plugin name.
# e.g. urltitle add youtube.com
has 'prefix' => (
		is => 'rw',
		isa => 'Str',
		required => 1,
	);

# A reference to a sub that returns a list of strings.
has 'list_get_handler' => (
		is => 'rw',
		isa => 'CodeRef',
		required => 1,
		traits => ['Code'],
		handles => {
			call_list_getter => 'execute',
		},
	);

# A reference to a sub that takes a list of strings to set the new list contents.
has 'list_set_handler' => (
		is => 'rw',
		isa => 'CodeRef',
		required => 1,
		traits => ['Code'],
		handles => {
			call_list_setter => 'execute',
		},
	);

# Do we need to be specifically addressing the bot for it to pick these up? Default true.
has 'requires_address' => (
		is => 'rw',
		isa => 'Bool',
		default => 1,
	);


# Oops
has 'undo_contents' => (
		is => 'rw',
		isa => 'ArrayRef',
		default => sub { [] },
	);

# Call this with the same args you normally get from said().
sub process_commands($$$$$$)
{
	my ($self, $bot, $channel, $who, $address, $said) = @_;

	# You probably need to specifically talk to the bot to do these commands.
	if ($self->requires_address()) {
		return unless $address;
	}
	
	my $prefix = $self->prefix();
	my $sir = sir($who);
	
	if ($said =~ /^($prefix list|list $prefix)$/i) {
		my @list = $self->call_list_getter();
		
		return "(empty list)" if @list == 0;
		my $response = "";
		for (my $i = 0; $i < @list; $i++) {
			$response .= "[$i] $list[$i] ";
		}
		if (length $response > 280) {
			$bot->say(channel => 'msg', who => $who, body => $response);
			return "The list is rather lengthy, $sir; I shall PM you the details.";
		}
		return $response;
	
	} elsif ($said =~ /^($prefix clear|clear $prefix)$/i) {
		my @list = $self->call_list_getter();
		
		if (@list > 0) {
			$self->undo_contents( [ @list ] );
			$self->call_list_setter();
			return "$prefix list cleared. Use 'undo clear' if this was in error.";
		} else {
			return "$prefix list is already empty.";
		}

	} elsif ($said =~ /^($prefix undo clear|undo clear $prefix)$/i) {
		my @list = $self->call_list_getter();
		my @undo_contents = @{ $self->undo_contents() };
		
		if (@list > 0) {
			return "$prefix list is not empty anymore; I don't belive I should clobber those changes, $sir.";
		} elsif (@undo_contents == 0) {
			return "$sir, I don't believe the $prefix list had anything on it yet.";
		} else {
			$self->call_list_setter(@undo_contents);
			return "$prefix list restored.";
		}

	} elsif ($said =~ /^($prefix add|add $prefix) (?<item>.+)$/i) {
		my @list = $self->call_list_getter();
		my $item = $+{item};
		
		if (grep { $_ eq $item } @list) {
			return "The list already contains that entry, $sir.";
		}
		
		push @list, $item;
		$self->call_list_setter(@list);
		return "$prefix entry '$item' added.";
	
	} elsif ($said =~ /^($prefix (del(ete)?|remove)|(del(ete)?|remove) $prefix) (?<item>.+)$/i) {
		my @list = $self->call_list_getter();
		my $item = $+{item};
		my $idx;
		
		if ($item =~ /^\[?(\d+)\]?$/) {
			# Remove by number.
			$idx = $1;
		} else {
			# Remove by value.
			$idx = firstidx { $_ eq $item } @list;
		}
		return "I don't believe the list has such an entry, $sir." unless $idx >= 0 && $idx < @list;
		
		# Replace offset 'idx', length 1, with emptylist
		my $removed = splice(@list, $idx, 1);
		$self->call_list_setter(@list);
		return "$prefix entry [$idx] '$removed' removed.";
	}
}


1;

