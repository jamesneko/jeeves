#
# Handles the loading and reloading of Jeeves plugins.
#
package Jeeves::PluginSystem;

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
use 5.018;

use Term::ANSIColor;
use File::Basename;
use Carp;
use Class::Load;
use Class::Unload;

require Exporter;
our @ISA = qw/Exporter/;
# use @EXPORT_OK to list the symbols to export on request only. @EXPORT is everything that gets exported by default.
our @EXPORT_OK = qw/load_plugins load_plugin reload_plugin unload_plugin unload_plugin_by_ref available_plugins loaded_plugins get_plugins get_plugin_by_name/;
our %EXPORT_TAGS = ( all => [@EXPORT_OK] );

use Jeeves::Plugin;
use Jeeves::PluginList;

our $PLUGINS_DIR;			# relative path
our $PLUGINS_PREFIX;		# e.g. Jeeves::Plugin
our %LOADED_PLUGINS;		# name => Plugin


sub load_plugins
{
	my ($pluginsdir, $pluginsprefix) = @_;
	$PLUGINS_DIR = $pluginsdir or croak "No plugins dir epecified.";
	croak "Plugins dir '$pluginsdir' is invalid." unless -d $PLUGINS_DIR;
	$PLUGINS_PREFIX = $pluginsprefix or croak "No plugins prefix specified.";
	
	my %available_plugins = available_plugins();
	say colored("Available plugins: ", 'bold cyan'), join(", ", sort keys %available_plugins), "\n";
	
	foreach my $pluginname (sort keys %available_plugins) {
		load_plugin($pluginname);
	}
}


# Load a plugin by name.
# Note that $pluginname is unsuitable for user input! Always check against available_plugins() first.
sub load_plugin
{
	my ($pluginname) = @_;
	my $filename = "${PLUGINS_DIR}/${pluginname}.pm";
	
	say colored("Loading: ", 'bold cyan'),
	    colored("$pluginname from '$filename'", 'cyan');
	
	{
		local $@;
		eval {
			Class::Load::load_class("${PLUGINS_PREFIX}::$pluginname");
		};
		if ($@) {
			say colored("Error while loading plugin '$pluginname': $@", 'bold red');
			return 0;
		}
	}

	my $plugin;
	
	{
		local $@;
		$plugin = eval "${PLUGINS_PREFIX}::${pluginname}->new()";
		if ($@) {
			say colored("Error while instantiating plugin '$pluginname': $@", 'bold red');
			return 0;
		}
		if ( ! $plugin) {
			say colored("Error while instantiating plugin '$pluginname': Not defined.", 'bold red');
			return 0;
		}
	}
	
	$LOADED_PLUGINS{$pluginname} = $plugin;
	
	$plugin->loaded($pluginname);
	say colored("Loaded:  ", 'bold cyan'),
	    colored("$pluginname", 'cyan');
	return 1;
}


# Reload a plugin (safely, slightly hackish)
sub reload_plugin
{
	my ($pluginname) = @_;
	my $plugin = $LOADED_PLUGINS{$pluginname};
	return unless $plugin;

	say colored("Reloading: ", 'bold cyan'),
	    colored("$pluginname", 'cyan');
	
	unload_plugin($pluginname);
	if (my $rval = load_plugin($pluginname)) {
		# All OK.
		say colored("Reloaded:  ", 'bold cyan'),
		    colored("$pluginname", 'cyan');
		return $rval;
	} else {
		# Damn. Revert as best we can.
		say colored("Reload failed:  ", 'bold red'),
		    colored("$pluginname", 'cyan');
		say colored("Patching plugin back in:  ", 'bold cyan'),
		    colored("$pluginname", 'cyan');

		$LOADED_PLUGINS{$pluginname} = $plugin;
		$plugin->loaded($pluginname);

		return $rval;
	}
}


# Unload a plugin by name.
# Does nothing if no such plugin is loaded.
sub unload_plugin
{
	my ($pluginname) = @_;

	my $plugin = $LOADED_PLUGINS{$pluginname} or return;
	
	say colored("Unloading: ", 'bold cyan'),
	    colored("$pluginname", 'cyan');

	delete $LOADED_PLUGINS{$pluginname};
		
	$plugin->unloaded($pluginname);

	Class::Unload->unload("${PLUGINS_PREFIX}::$pluginname");
	# say "%INC = {", join(',', keys %INC), "}";

	say colored("Unloaded:  ", 'bold cyan'),
	    colored("$pluginname", 'cyan');
}

sub unload_plugin_by_ref
{
	my ($pluginref) = @_;
	while (my ($pluginname, $plugin) = each %LOADED_PLUGINS) {
		if ($plugin == $pluginref) {
			return unload_plugin($pluginname);
		}
	}
}


# Tell us what plugins can be loaded/unloaded.
# Returns a list of pairs; the basenames => the relative filenames to .pm files which can be used with load_plugin().
sub available_plugins
{
	# See perldoc -f glob; basically, we need to pass quotes because our plugins dir may be spacey.
	my @available_paths = glob qq("${PLUGINS_DIR}/*.pm");

	return map { basename($_, '.pm') => $_ } @available_paths;
}


# Just lists the plugins that are currently loaded.
# If you want to *do* anything to them, you should get a PluginList via get_plugins().
sub loaded_plugins
{
	return sort keys %LOADED_PLUGINS;
}


sub get_plugins
{
	my $pluginlist = Jeeves::PluginList->new();
	foreach my $plugin (values %LOADED_PLUGINS) {
		$pluginlist->add($plugin);
	}
	return $pluginlist;
}


sub get_plugin_by_name
{
	my ($name) = @_;
	return $LOADED_PLUGINS{$name};
}


1;

