# Jeeves #

Jeeves is an IRC robot I run in my friends' favourite IRC channel. He serves lemonade, and will convert currencies *(mentioned with three-letter code such as USD)* and times *(using the TZ database identifiers such as America/Los_Angeles, with a few commonly-used shortcuts)*. He can be configured to emit the page title of particular URLs *(e.g. youtube)* in chat.

He is built on top of [Bot::BasicBot](http://metacpan.org/pod/Bot::BasicBot) and is not exactly general-purpose; while not my best work, I may as well make his source publicly viewable in case anyone has similar ideas and wants to see how he works. I am slowly migrating my code to a 'Plugin' system so I can hot-reload modules without the bot quitting the server.