# Necessary to pull the tag 'buster' rather than e.g. 'stable', because docker is a piece of shit that will keep the :stable or even :latest tag around without checking the server for a newer version of that tag.
FROM debian:buster

RUN echo 'APT::Get::Assume-Yes "true";' > /etc/apt/apt.conf.d/90-assume-yes
RUN apt update && apt install curl perl cpanminus carton build-essential python

RUN mkdir /app
WORKDIR /app

# DOCKER 'COPY' IS A BIG OL' PILE OF TURDS
COPY libs/ ./libs/
COPY deps/youtube-dl /usr/local/bin/
COPY cpanfile cpanfile.snapshot Jeeves.pl run.sh test.sh ./
RUN ln -s /data/ ./mem

RUN carton install

ENTRYPOINT ["./run.sh"]

