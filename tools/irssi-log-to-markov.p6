#!/usr/bin/env perl6

use v6;

use Terminal::ANSIColor;


multi sub MAIN(Str :$output!, *@logfiles) {
	die "Feed me irssi log files! These ones don't exist!" unless all(@logfiles».IO) ~~ :e;
	
	my @utterances;
	for @logfiles -> $filename {
		note colored("Parsing: $filename", 'cyan');
		@utterances.append(parse-irssi-file($filename));
	};

	note colored("Preprocessing...", 'yellow');
	for @utterances.race -> $utt {
		preprocess-utterance($utt);
	}

	note colored("Digesting...", 'yellow');
	my %ngrams = digest-sentence-ngrams(@utterances.flat);
	note "";

	for 1..5 -> $i {
		my $seed-utt = %ngrams.values.pick;
		note colored "Random text $i: " ~ random-text(%ngrams, $seed-utt), 'green';
	}

	note colored "Writing to '$output'...", 'bold cyan';
	write-ngrams-json(%ngrams, $output);
}


class Utterance {
	has Int $.time is rw;		# for 'context' ngrams for decrypting challenge -> response, we'd want to look at groups of utterances occuring close together.
	has Str $.who is rw;
	has Str $.at is rw;
	has Str $.line is rw;

	method Str {
		if $.at {
			return "$.time <$.who> $.at: $.line";
		} else {
			return "$.time <$.who> $.line";
		}
	}
}


class NGram {
	has Str @.preceeding;	# list of words
	has Int %.yields;			# hash of last word => count

	method add(Str $yielded-word) {
		%!yields{$yielded-word}++;
	}

	method Str {
		my $s = "[" ~ @.preceeding ~ "] ==> " ~ join(' ', self.yields.pairs.map: { .key ~ "(" ~ .value ~ ")" });
		return $s;
	}
}


grammar IrssiLogLine {
	regex time { (\d\d) ':' (\d\d) }
	regex who  { '<' '@'? ' '? (\S+) '>' }
	regex at   { (<-[ :]>+) ':' }
	regex line { .* }
	rule TOP   { ^^ <time> <who> <at>? <line> $$ }
}
class IrssiLogLineActions {
	method TOP($/) {
		if $<at> {
			make Utterance.new(
					time => $<time>[0] * 60 + $<time>[1],
					who => $<who>[0].Str,
					at => $<at>[0].Str,
					line => $<line>.Str
				);
		} else {
			make Utterance.new(
					time => $<time>[0] * 60 + $<time>[1],
					who => $<who>[0].Str,
					line => $<line>.Str
				);
		}
	}
}


sub parse-irssi-file(Str $filename) {
	my @utterances;
	my $fh = open $filename;
	while (my $line = $fh.get) {
		my $utt = parse-irssi-line($line);
		push @utterances, $utt if $utt;
	}
	$fh.close;
	return @utterances;
}


sub parse-irssi-line(Str $str) returns Utterance {
	if ($str ~~ / ^^ '---' /) {
		#say colored($str, 'blue');
		return;

	} elsif ($str ~~ / ^^ \d\d ':' \d\d \s+ '-!-' /) {
		#say colored($str, 'cyan');
		return;

	} elsif (my $match = IrssiLogLine.parse($str, actions => IrssiLogLineActions)) {
		#say $match.made.Str;
		return $match.made;
		
	} else {
		return Nil;
	}
}


sub preprocess-utterance(Utterance:D $utt) {
	$utt.who  = preprocess-text($utt.who);
	$utt.at   = preprocess-text($utt.at) if $utt.at;
	$utt.line = preprocess-text($utt.line);
}


sub preprocess-text(Str:D $str is copy) returns Str:D {
	given $str {
		# Remove trailing 1s and underscores, typically from usernames.
		s:g:i/ <[1_]> >> //;

		# Remove URLs.
		s:g:i/ 'http' s? '://' \S* //;

		# Everyone knows robots are bad with contractions.
		## s:g/ "'d" >> / would/;	# ah shit, I'd -> I did / I would based on context, we are fucked.
		s:g:i/ "n't" >> / not/;
		s:g:i/ << "I'm" >> /I am/;
		s:g:i/ "'ll" >> / will/;
		s:g:i/ "'ve" >> / have/;
		s:g:i/ "'re" >> / are/;
	}
	return $str;
}


sub tokenise(Str:D $str) {
	return $str.comb(/ <[:;=]><[)(D]> || 'D:' || <[\w'_-]>+ || <[.,?!/]>+ /);
}


constant NGRAM_SIZE = 2;
sub digest-sentence-ngrams(@utterances) {
	my %ngrams;	# "word1 word2" => NGram

	for @utterances -> $utt {
		# Split words out and decorate them with a start and end special token.
		my @words = "[SPEAKER:" ~ $utt.who ~ "]", |tokenise($utt.line), "[EOL]";
		for 0..^@words - NGRAM_SIZE -> $i {
			my @gram = @words[$i..^$i+NGRAM_SIZE+1];
			my $yields = pop @gram;
			my $key = @gram.join(' ');

			%ngrams{$key} //= NGram.new(preceeding => @gram);
			%ngrams{$key}.add($yields.Str);
		}
	}
	return %ngrams;
}


sub random-text(%ngrams, NGram $initial-ngram) {
	my @line = $initial-ngram.preceeding;
	my $word;
	while (@line < 30) {
		my $last-key = @line[*-NGRAM_SIZE..*-1].join(' ');
		my $last-ngram = %ngrams{$last-key};
		$word = next-random-word($last-ngram);
		last if $word eq "[EOL]";
		push @line, $word;
	}
	return @line.join(" ");
}


#= Look at $utt, randomly pick one of its successors.
sub next-random-word(NGram $ngram) {
	# Fill an array with copies of the utterance's possible successors, weighted according to their count.
	my @words;
	for $ngram.yields.kv -> $word, $count {
		@words.append($word xx $count);
	}
	return @words.pick;
}


sub write-ngrams-json(%ngrams, $output) {
	my $fh = open($output, :w);
	$fh.say('{');
	for %ngrams.kv -> $key, $ngram {
		$fh.print(" " ~ json-escaped-string($key) ~ ' : { ');
		$fh.print(join ', ', $ngram.yields.pairs.map: {
			json-escaped-string(.key) ~ ": " ~ .value
		});
		$fh.print(' },' ~ "\n");
	}
	$fh.say('}');
	$fh.close;
}


# Because JSON::Tiny (and IO in general) is INSANELY SLOW IN PERL6 ATM OMG
sub json-escaped-string(Str $str) {
	$str ~~ s:g/'\\'/\\\\/;
	$str ~~ s:g/'"'/\\"/;
	return qq/"$str"/;
}

