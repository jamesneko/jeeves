# Core
requires 'perl', 'v5.18';
requires 'Bot::BasicBot';
requires 'Moose';
requires 'JSON';
requires 'Try::Tiny'; # huh, apparently I used two different ones, in TimeZone and PluginList.
requires 'Class::Load';
requires 'Class::Unload';

# Plugins
requires 'URI';
requires 'URI::Title';
requires 'List::MoreUtils';
requires 'Finance::Quote';
requires 'Forex';
requires 'DateTime';
requires 'TryCatch';
requires 'JSON::API';
requires 'Number::Format';

# WowGuildApi
requires 'LWP::Authen::OAuth2';
requires 'Method::Signatures';

# Hidden C Header dependencies:
# Finance::Quote ultimately pulls in Net::SSLeay, which needs to compile against openssl.
# Debian: sudo apt-get install libssl-dev
